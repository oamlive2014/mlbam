# MLBAM
---

**mlbam** allows you to download MLBAM data into Python dataframes that can be easily analyzed for your own needs.

## Main Features
---

**mlbam** currently has the following capabilities:

- Creates a summary of the game and teams involved.

## Installation Instructions
---

The source code is currently hosted on BitBucket at: 
http://bitbucket.org/oamlive2014/mlbam/

Easiest way to install is to clone the repository into a folder of your choice. Use the following command:
    
```sh
git clone https://bitbucket.org/oamlive2014/mlbam/
```