# -*- coding: utf-8 -*-
"""
Created on Sun Aug 03 14:48:11 2014

@author: Richard
"""

import datetime as dt
import numpy as np
import pandas as pd
import workdays as wd
from dateutil.relativedelta import relativedelta

# Function to format date strings to date values
def Get_Date(dt_input, str_format=''):
    """This function returns the date from a text.
    
    Args:
        dt_input (str): Date in a string format.
        str_format (str, optional): The format that the text is in. Defaults to 'YYYYMMDD'.
    
    Returns:
        A pandas timestamp object.
    """
    if type(dt_input) is not pd.datetime.date:
        # Make the format into all upper case
        str_format = str_format.upper()
        
        if str_format == 'YYYYMMDD':
            dt_output = pd.to_datetime(dt_input)
        elif str_format == 'DDMMMYYYY':
            dt_output = pd.to_datetime(dt_input, format='%d%b%Y')
        elif str_format == 'M/D/YYYY':
            dt_output = pd.to_datetime(dt_input, format='%m/%d/%Y')
        else:
            dt_output = pd.to_datetime(dt_input, infer_datetime_format=True)
            
    else:
        dt_output = dt_input
        
    return dt_output

def Get_Date_String(dt_input, str_format='%m/%d/%Y'):
    """Converts various date value type to a string (for SQL)
    Args:
        dt_input (any): date value to be converted
        str_format (str, optional): format in which the date should be converted to, defaults to '%m/%d/%Y'
        
    Yields:
        str: the date formatted as string in the specified format.
        
    """                   
    if type(dt_input) is dt.date or type(dt_input) is dt.datetime:          # if dt_start is formatted as a python date object it needs to be converted to a string
        dt_output = dt_input.strftime(str_format)
    else:
        dt_output = dt_input

    return dt_output