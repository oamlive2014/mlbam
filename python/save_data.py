# -*- coding: utf-8 -*-
"""
Created on Sun Aug 09 19:57:04 2015

@author: Jihae
"""

import os
directory = os.path.dirname(__file__)

import datetime as dt
import numpy as np
import pandas as pd
import urllib2
import warnings
import yaml
from io import BytesIO

import date_functions as dtf
import single_game as sg
import xml_parser as xp

class SingleGame:
    def __init__(self, game_id):
        """Loads the data for a single game.
        
        The following dataframes are created:
            (1) Pitch-by-pitch data
            (2) Events data
            (3) Players-level boxscore data
            (4) Coach data
            (5) Umpire data
            (6) Game information
            (7) Game results
        
        Args:
            game_id (str): Game ID
            
        """
        self.game_id = game_id
        
        # SingleGame instance
        self.sg_inst = sg.SingleGame(game_id)
        
    def get_data(self):
        """Downloads the needed XMLs and saves the data into dataframes.
        """
        # Save away the value needed locally
        sg_inst = self.sg_inst
        
        # Run the prep function
        sg_inst.prep_all()
        
        # Error value...
        self.error_value = sg_inst.error_value
        
        # Game final flag...
        self.final_flg = sg_inst.final_flg
        
        # Game Type
        if hasattr(sg_inst, 'game_type'):
            self.game_type = sg_inst.game_type
        else:
            self.game_type = ''
        
        # Save away the needed dataframes, if no errors and depending on the final flag
        if sg_inst.error_value == '':
            self.game = sg_inst.df_game
            self.coach = sg_inst.df_coach
            self.umpire = sg_inst.df_umpire
            
            if self.final_flg:
                self.pitch = sg_inst.df_pitch
                self.events = sg_inst.df_events
                self.players = sg_inst.df_players                
                self.results = sg_inst.df_game_results
            
        return self
        
    def save_data(self, folder):
        """Save the dataframes into CSV files to a given folder.
        
        Args:
            folder (str): Folder to save the csv files to.
            
        """
        # Run the get_data function first if it hasn't already run.
        if not hasattr(self, 'error_value'):
            self.get_data()
        
        # If no errors, output into CSV
        if self.error_value == '':
            # Check to see if the folder exists; if not, make it.
            if not os.path.isdir(folder):
                os.makedirs(folder)
                    
            if self.final_flg:
                # Output everything
                self.pitch.to_csv(os.path.join(folder, 'pitch_%s.csv'%(self.game_id)), index=False, sep=',')
                self.events.to_csv(os.path.join(folder, 'events_%s.csv'%(self.game_id)), index=False, sep=',')
                self.players.to_csv(os.path.join(folder, 'players_%s.csv'%(self.game_id)), index=False, sep=',')
                self.coach.to_csv(os.path.join(folder, 'coach_%s.csv'%(self.game_id)), index=False, sep=',')
                self.umpire.to_csv(os.path.join(folder, 'umpire_%s.csv'%(self.game_id)), index=False, sep=',')
                self.game.to_csv(os.path.join(folder, 'game_%s.csv'%(self.game_id)), index=False, sep=',')
                self.results.to_csv(os.path.join(folder, 'results_%s.csv'%(self.game_id)), index=False, sep=',')
                
            else:
                # The game isn't final... Just output the game information
                self.game.to_csv(os.path.join(folder, 'game_%s.csv'%(self.game_id)), index=False, sep=',')
                
                
class SingleDate:
    def __init__(self, game_date, game_type_list=['R', 'S', 'E', 'A', 'F', 'D', 'L', 'W']):
        """Loads the data for a single game date.
        
        Args:
            game_date (str or date): Date to pull games for.
            game_type_list (list, optional): List of game types to pull; defaults to all.
            
        """
        # Get the game date
        self.game_date = dtf.Get_Date(game_date).date()
        self.str_game_date = dtf.Get_Date_String(self.game_date, '%Y%m%d')
        
        # First, check the season year.
        # Currently, this code has been backtested to only 2010.
        # Raise a warning if before 2010; raise an error if before 1933 (first confirmed year in MLBAM)
        assert self.game_date.year >= 1933, 'No MLBAM data exists for years before 1933.'
        if self.game_date.year < 2010:
            warnings.warn('This code has not been tested for seasons before 2010. Please be mindful of errors.')
        
        # Game type list
        self.game_type_list = game_type_list
        
        # Configs
        self._get_configs()
        
        # Grab the list of games
        self._get_game_list()
        
        # Flag to keep track of whether the data was loaded
        self.data_load_flg = False
        
    def _get_configs(self):
        """Gets all relevant configuration information.
        """        
        # Baseball Configs
        with open(os.path.join(directory, 'mlbam_configurations.yml'), 'r') as f:
            configs = yaml.load(f)                        
            f.close()
        
        # Base URL...
        base_url = configs['base_url']
        
        # Save away the values needed
        self.base_url = base_url
        
        return self
        
    def _get_game_list(self):
        """Gets a list of all game ID's for the given date.
        """
        # Get the URL of the date folder
        url_game = '%syear_%04d/month_%02d/day_%02d/'%(self.base_url, self.game_date.year, self.game_date.month, self.game_date.day)
                
        # Open up the site and get a list of folders
        req = urllib2.Request(url_game)
        res = urllib2.urlopen(req)
        html = res.read()
        
        xps = xp.XMLParser(BytesIO(html), document_id=0, html_flg=True)
        xps.xml_parser()
        
        try:
            links = xps.dict_structure['a']
        except KeyError:
            link_list = []
        else:
            link_list = links.loc[:, 'href']
            link_list = [x[:-1] for x in link_list if x[:14] == 'gid_%04d_%02d_%02d'%(self.game_date.year, self.game_date.month, self.game_date.day)]
        
        self.game_list = list(np.unique(link_list))
        
        return self
        
    def get_data(self):
        """Gets the data for the specific date into dataframes.
        
        The following dataframes are created for all games in final status:
            (1) Pitch-by-pitch data
            (2) Events data
            (3) Players-level boxscore data
            (4) Coach data
            (5) Umpire data
            (6) Game information
            (7) Game results
            
        """
        # List of empty dataframes
        pitch = pd.DataFrame()
        events = pd.DataFrame()
        players = pd.DataFrame()
        coach = pd.DataFrame()
        umpire = pd.DataFrame()
        game = pd.DataFrame()
        results = pd.DataFrame()
        
        # Loop through the game list
        for i in self.game_list:
            # Grab the data
            try:
                sg_inst = SingleGame(i)
            except Exception as e:
                # Print out a warning and continue to the next game ID
                warnings.warn('An error type %s in loading ID %s; Message: %s'%(type(e), i, e.message))
                continue
            
            # Let's grab the data
            sg_inst.get_data()
            
            # Continue processing if there was no error and if game is final
            if sg_inst.error_value == '' and sg_inst.final_flg and sg_inst.game_type in self.game_type_list:
                pitch = pd.concat(objs=[pitch, sg_inst.pitch])
                events = pd.concat(objs=[events, sg_inst.events])
                players = pd.concat(objs=[players, sg_inst.players])                
                game = pd.concat(objs=[game, sg_inst.game])
                results = pd.concat(objs=[results, sg_inst.results])
                coach = pd.concat(objs=[coach, sg_inst.coach])
                umpire = pd.concat(objs=[umpire, sg_inst.umpire])
                
            else:
                # Print out a warning and continue to the next game ID
                warnings.warn('Skipping ID %s; Error Value: %s, Final Flag: %s, Game Type: %s'%(i, sg_inst.error_value, sg_inst.final_flg, sg_inst.game_type))
                continue
        
        # Data Load is now True
        self.data_load_flg = True
        
        # Save back to the instance
        self.pitch = pitch
        self.events = events
        self.players = players
        self.coach = coach
        self.umpire = umpire
        self.game = game
        self.results = results
        
        return self
        
    def save_data(self, folder):
        """Save the dataframes into CSV files to a given folder.
        
        Args:
            folder (str): Folder to save the csv files to.
            
        """
        # Run the get_data function first if it hasn't already run.
        if not self.data_load_flg:
            self.get_data()
                
        # Output everything, if there were game on the date
        if len(self.game) > 0:
            # Check to see if the folder exists; if not, make it.
            if not os.path.isdir(folder):
                os.makedirs(folder)
            
            self.pitch.to_csv(os.path.join(folder, 'pitch_%s.csv'%(self.str_game_date)), index=False, sep=',')
            self.events.to_csv(os.path.join(folder, 'events_%s.csv'%(self.str_game_date)), index=False, sep=',')
            self.players.to_csv(os.path.join(folder, 'players_%s.csv'%(self.str_game_date)), index=False, sep=',')
            self.coach.to_csv(os.path.join(folder, 'coach_%s.csv'%(self.str_game_date)), index=False, sep=',')
            self.umpire.to_csv(os.path.join(folder, 'umpire_%s.csv'%(self.str_game_date)), index=False, sep=',')
            self.game.to_csv(os.path.join(folder, 'game_%s.csv'%(self.str_game_date)), index=False, sep=',')
            self.results.to_csv(os.path.join(folder, 'results_%s.csv'%(self.str_game_date)), index=False, sep=',')
            
        else:
            warnings.warn('There were no games downloaded for date: %s'%(self.str_game_date))
        
class SingleSeason:
    def __init__(self, season_year, start_date=None, end_date=None, 
                 regular_flg=True, spring_flg=True, allstar_flg=True, playoff_flg=True):
        """Saves away the CSV data for a full season.
        
        Args:
            season_year (int): Year of season to pull data for.
            start_date (str or date): Date to start with during the season.
            end_date (str or date): Date to end with during the season.
            regular_flg (bln, optional): True to pull regular season games.
            spring_flg (bln, optional): True to pull spring training and exhibition games.
            allstar_flg (bln, optional): True to pull all-star game.
            playoff_flg (bln, optional): True to pull playoff games.
            
        """
        # First, check the season year.
        # Currently, this code has been backtested to only 2010.
        # Raise a warning if before 2010; raise an error if before 1933 (first confirmed year in MLBAM)
        assert season_year >= 1933, 'No MLBAM data exists for years before 1933.'
        if season_year < 2010:
            warnings.warn('This code has not been tested for seasons before 2010. Please be mindful of errors.')
            
        # Save away values locally
        self.season_year = season_year
        
        # Get the start date
        if start_date is not None:
            self.start_date = dtf.Get_Date(start_date).date()
            assert self.start_date.year == season_year, 'The start date given must be within the same season year.'
        else:
            self.start_date = dt.date(self.season_year, 1, 1)
            
        # Get the end date
        if end_date is not None:
            self.end_date = dtf.Get_Date(end_date).date()
            assert self.end_date.year == season_year, 'The end date given must be within the same season year.'
        else:
            self.end_date = dt.date(self.season_year, 12, 31)
        
        # Get the list of game types
        game_type_list = []
        
        if regular_flg:
            game_type_list.append('R')
            
        if spring_flg:
            game_type_list.extend(['S', 'E'])
            
        if allstar_flg:
            game_type_list.append('A')
            
        if playoff_flg:
            game_type_list.extend(['F', 'D', 'L', 'W'])
            
        self.game_type_list = game_type_list
        
    def save_data(self, folder):
        """Save the dataframes into CSV files to a given folder.
        A folder gets created for every date and the dataframes are saved in each individual folder.
        
        Args:
            folder (str): Folder to save the csv files to.
            
        """
        # Get all dates for the year.
        # Start with 1/1 and loop to the end.
        game_date = self.start_date
        while game_date <= self.end_date:
            full_folder = os.path.join(folder, '%d'%(self.season_year), dtf.Get_Date_String(game_date, '%Y%m%d'))
            sd = SingleDate(game_date, self.game_type_list)
            sd.save_data(full_folder)
            
            # Move to the next date
            game_date += dt.timedelta(days=1)
            
        return self
        
    def combine_data(self, folder):
        """Combines the CSV data in the individual date folders (from the save_data function).
        Dataframes are created in the main folder.
        
        Args:
            folder (str): Folder that contains the individual date folders.
            
        """
        # Season folder should be:
        season_folder = os.path.join(folder, '%d'%(self.season_year))
        
        # Dictionary that holds the pandas dataframes
        dict_data = {
                'pitch': pd.DataFrame(), 
                'events': pd.DataFrame(), 
                'players': pd.DataFrame(), 
                'coach': pd.DataFrame(), 
                'umpire': pd.DataFrame(), 
                'game': pd.DataFrame(), 
                'results': pd.DataFrame(), 
            }
            
        # Loop through each individual folder
        for d in sorted(os.listdir(season_folder)):
            if os.path.isdir(os.path.join(season_folder, d)):
                for k in dict_data.keys():
                    # Read in the CSV file
                    df = pd.read_csv(os.path.join(season_folder, d, '%s_%s.csv'%(k, d)), sep=',')
                    dict_data[k] = pd.concat(objs=[dict_data[k], df])
                    
        # Output the files
        for k in dict_data.keys():
            dict_data[k].to_csv(os.path.join(folder, '%s_%d.csv'%(k, self.season_year)), index=False, sep=',')
        
        # Save back to instance
        self.dict_data = dict_data
        
        return self
        
        
if __name__ == '__main__':
    #sd = SingleDate('2015-08-09')
    #sd._get_game_list()
    #print sd.game_list    
    #sd.save_data(os.path.join(directory, '../output'))
        
    for season_year in xrange(2014, 1999, -1):
        ss = SingleSeason(season_year)
        ss.save_data(os.path.join(directory, '../output'))
        #ss.combine_data(os.path.join(directory, '../output'))
    
    #ss = SingleSeason(2015, start_date='2015-03-22', end_date='2015-08-12')
    #ss.save_data(os.path.join(directory, '../output'))