# -*- coding: utf-8 -*-
"""
Created on Tue Dec 30 12:02:44 2014

@author: jhwang1
"""

import lxml.etree as et
import pandas as pd

class XMLParser:
    def __init__(self, xml_path, xsd_path=None, document_id=None, 
                 html_flg=False, remove_comments=False, recover_flg=False):
        """Initializes an instance of XMLParser
        
        Args:
            xml_path (str): Path to the XML.
            xsd_path (str): Path to the XSD (Schema File).
        
        """
        # Save away the values
        self.xml_path = xml_path
        self.xsd_path = xsd_path
        self.document_id = document_id
        self.html_flg = html_flg
        self.remove_comments = remove_comments
        self.recover_flg = recover_flg
        
        # Load the XML
        self.load_xml_file()
        
        # Create a dictionary that holds the parent-child relationships
        self.table_relationship = {}
        
        # Create a dictionary structure that will hold the values
        self.dict_structure = {}
    
    def load_xml_file(self):
        """Loads the XML file.
        
        Returns:
            ElementTree object with the loaded xML
        
        """
        # Save away path locally
        xml_path = self.xml_path
        xsd_path = self.xsd_path
        html_flg = self.html_flg
        remove_comments = self.remove_comments
        recover_flg = self.recover_flg
        
        # Return an ElementTree object
        # Use some default settings to get a cleaner parsing
        if xsd_path is not None:
            xsd = et.XMLSchema(et.parse(xsd_path))
            parser = et.XMLParser(schema=xsd, remove_comments=remove_comments, 
                                  recover=recover_flg, ns_clean=True)
        else:
            if html_flg:
                parser = et.HTMLParser()
            else:
                parser = et.XMLParser(remove_comments=remove_comments, 
                                      recover=recover_flg, ns_clean=True)
            
        tree = et.parse(xml_path, parser)
        
        self.tree = tree
        
        return self     
                
    def xml_parser(self):
        # Function that will be used in recursive manner to get all XML information
        def create_all_xml_data(dict_structure, table_relationship, xml_document_id, 
                                element, counter, parent_id=None, element_id=1, level=1):
            # Function that grabs all attributes, tag name, and value
            def get_all_info(element, xml_document_id, counter):            
                # Get the name, attributes, and the value
                # Ignore namespaces when getting the tag name and get lower case
                name = et.QName(element.tag).localname.lower()
                #name = element.tag.split(r'}')[-1].lower()
                attributes = element.attrib
                value = element.text
                
                # Create a dictionary to hold the values. Include xml_document_id and counter
                all_values = {'xml_document_id': xml_document_id, 'tag_counter': counter}
                
                # Strip out whitespaces
                try:
                    value = value.strip()
                except:
                    pass
                
                # Update dictionary with the attributes
                # Make all attribute names lower case
                for key in attributes:
                    key_clean = et.QName(key).localname.lower()
                    all_values.update({key_clean: attributes[key]})
                
                # Update the dictionary with the value
                all_values.update({'text_value': value})
                
                return name, all_values
                
            # Get all information within the current element            
            element_name, element_attrib = get_all_info(element, xml_document_id, counter)
            
            # Add in the parent ID if needed
            if parent_id is not None:
                # Get the parent tag
                parent_tag_name = et.QName(element.getparent().tag).localname.lower()
                #parent_tag_name = element.getparent().tag.split(r'}')[-1].lower()
                key_value = (parent_tag_name + '_id')
                
                if key_value not in element_attrib.keys():
                    element_attrib.update({(key_value): parent_id})
                else:
                    element_attrib.update({(key_value + '_xml'): element_attrib[key_value]})
                    element_attrib.update({(key_value): parent_id})
                
                # Update the table structure
                if element_name not in table_relationship.keys():
                    table_relationship.update({element_name: {'level': set(), 'parent': set()}})
                
                table_relationship[element_name]['parent'].add(parent_tag_name)
                table_relationship[element_name]['level'].add(level)
            
            # Add in the element ID
            key_value = (element_name + '_id')
            
            if key_value not in element_attrib.keys():
                element_attrib.update({(key_value): element_id})
            else:
                element_attrib.update({(key_value + '_xml'): element_attrib[key_value]})
                element_attrib.update({(key_value): element_id})
            
            # Update the master dictionary list
            if element_name not in dict_structure.keys():
                dict_structure.update({element_name: []})
                
            dict_structure[element_name].append(element_attrib)
                
            # If it has more children, call the function recursively
            if len(element.getchildren()) > 0:
                for i in xrange(len(element.getchildren())):
                    # Increase the counter by 1
                    counter += 1
                    
                    # Get the child information
                    child = element.getchildren()[i]
                    child_name = et.QName(child.tag).localname.lower()
                    child_name = child.tag.split(r'}')[-1].lower()
                    
                    # Figure out the element_id = len + 1
                    if child_name in dict_structure.keys():
                        child_id = len(dict_structure[child_name]) + 1
                    else:
                        child_id = 1
                    
                    # Call the function recursively
                    dict_structure, table_relationship, counter = create_all_xml_data(dict_structure, 
                                                                                      table_relationship, 
                                                                                      xml_document_id,
                                                                                      element=child, 
                                                                                      counter=counter, 
                                                                                      parent_id=element_id, 
                                                                                      element_id=child_id, 
                                                                                      level=(level + 1))
            
            return dict_structure, table_relationship, counter
        
        # Local copies of the values needed
        tree = self.tree
        document_id = self.document_id
        
        # Create the dictionary structure that will hold the values
        dict_structure = self.dict_structure
        table_relationship = self.table_relationship
        
        # Counter to assign to every tag
        counter = 1
        
        # Get the root and clean namespaces
        root = tree.getroot()
        
        # Run the function to get all values
        dict_structure, table_relationship, counter = create_all_xml_data(dict_structure, table_relationship, 
                                                                          document_id, root, counter)
        
        # Make the list into pandas dataframes
        for key in dict_structure:
            dict_list = dict_structure[key]
            data = pd.DataFrame(data=dict_list)
            #data.set_index([key + '_id'], inplace=True, verify_integrity=True)
            
            dict_structure[key] = data
        
        # Save back to instance
        self.dict_structure = dict_structure
        self.table_relationship = table_relationship
        self.number_tags = counter
        
        return self