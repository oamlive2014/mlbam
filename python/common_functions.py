# -*- coding: utf-8 -*-
"""
Created on Tue Jan 20 20:39:05 2015

@author: Jihae
"""

import warnings
import pandas as pd
import numpy as np
import dateutil as du

def get_ratio(f1, f2, default=np.NaN):
    """Gets the ratio between f1 and f2 (f1 / f2).
    
    Args:
        f1 (float): Number to divide
        f2 (float): Number to divide by
        default: Default value if divison throws an error
    
    Returns:
        Ratio value
        
    """
    value = default
    
    try:
        value = float(f1) / float(f2)
    except ZeroDivisionError:
        warnings.warn('Tried to divide by zero.')
        pass
    
    return value
    
# Function to get the value from a dictionary by key
def dict_value(d, k, default=None):
    """Gets the value from a dictionary by key.
    
    Args:
        d (dict): Dictionary
        k: Key to use        
        default (optional): Default value to pass back
    
    Returns:
        Value from dictionary
    
    """
    value = default
    
    try:
        value = d[k]
    except KeyError:
        pass
    
    return value
    
def df_value(df, ix, col, default=None):
    """Gets the value from a dataframe by index and column.
    
    Args:
        df (dataframe): Pandas dataframe
        ix: Index
        col: Column
        default (optional): Default value to pass back
    
    Returns:
        Value from dataframe
    
    """
    # Value to the returned
    value = default
    
    try:
        value = df.loc[ix, col]
    except KeyError:
        pass
    
    return value
            
def coalesce(*args):
    """Gets the first non-null value in a list.
    
    Args:
        args (list): List
    
    Returns:
        First non-null value
    
    """
    # Value to be returned
    value = None
    
    # Loop through...
    for i in xrange(len(args)):
        item = args[i]
        
        if pd.notnull(item):
            value = item
            break
    
    return value
    
def convert_to_int(text, default=np.NaN):
    """Converts the value to integer.
    
    Args:
        value: Value to change to integer
        default: Default value if there is an error in conversion
    
    Returns:
        Integer value or NaN
    
    """
    # Value to be returned
    value = default
    
    try:
        value = int(text)
    except:        
        pass
    
    return value
    
def convert_to_date(text, default=pd.NaT):
    """Converts the value to date.
    
    Args:
        value: Value to change to date
        default: Default value if there is an error in conversion
    
    Returns:
        Integer value or NaN
    
    """
    # Value to be returned
    value = default
    
    if pd.isnull(text):
        return value
    
    try:
        # Only run the parser if there is some string
        if str(text).strip() != '':
            value = du.parser.parse(text)
    except:        
        pass
    
    return value