# -*- coding: utf-8 -*-
"""
Created on Mon Jan 19 16:55:20 2015

@author: Jihae
"""

import os
directory = os.path.dirname(__file__)

import sys
import datetime as dt
import logging
import logging.handlers
import numpy as np
import pandas as pd
import requests
import yaml
import warnings

import common_functions as cf
import xml_parser as xp

class SingleGame:
    """Class that allows download of a single game.
    
    It loads all related XML's for one game and then simplies the relevant structure
    to summarize into dataframe(s).
    
    """
    def __init__(self, game_id, verbose=False):
        """Initializes an instance.
        
        Args:
            game_id (str): MLBAM Game ID to pull
            verbose (bln, optional): True to see logging; Defaults to False.
        
        """
        # Save away the values
        self.game_id = game_id
        self.verbose = verbose
        
        # Validate the Game ID
        self._validate_game_id()
        
        # Get the configurations
        self._get_configs()
        
        # Set up the logger
        self._set_up_logger()
        
    def _validate_game_id(self):
        """Validates the game ID received.
        """
        # Save away the values needed locally
        game_id = self.game_id
        
        # Check for length of game_id. It should be 30 characters in length.
        assert len(game_id) == 30, 'The game_id provided (%s) is not valid!'%(game_id)
        
        # Now, let's split the ID by underscore to get the pieces needed
        game_id_parts = game_id.split('_')
        
        # Length of the split should be 7
        assert len(game_id_parts) == 7, 'The game_id provided (%s) is not valid!'%(game_id)
        
        # Save away the parsed parts
        self.game_date = dt.date(int(game_id_parts[1]), int(game_id_parts[2]), int(game_id_parts[3]))
        self.game_number = int(game_id_parts[6])
        
        return self        
        
    def _get_configs(self):
        """Gets the relevant configurations.
        """
        # Save away values needed
        game_date = self.game_date
        game_id = self.game_id
        
        # Config file
        with open(os.path.join(directory, 'mlbam_configurations.yml'), 'r') as f:
            configs = yaml.load(f)
            f.close()
        
        # Configs for URL
        self.base_url = configs['base_url']
        self.statcast_url = configs['statcast_url']
        self.game_url = '%syear_%04d/month_%02d/day_%02d/%s/'%(configs['base_url'], \
            game_date.year, game_date.month, game_date.day, game_id)
            
        # Various configs for XML data
        self.game_configs = configs['game']
        self.players_configs = configs['players']
        self.boxscore_configs = configs['boxscore']
        self.events_configs = configs['events']
        
        return self
        
    def _set_up_logger(self):
        """Sets up the logger.
        """
        # Save away values needed locally
        verbose = self.verbose
        
        # Set up the logger
        logger = logging.getLogger('mlbam_single_game_%s'%(self.game_id))
        
        # Console handler
        ch = logging.StreamHandler()
        if verbose:
            ch.setLevel(logging.DEBUG)
        else:
            ch.setLevel(logging.CRITICAL)
        
        # Formatter
        cf = logging.Formatter('%(asctime)s - %(name)-12s: %(levelname)-8s %(message)s')
        ch.setFormatter(cf)
        
        logger.addHandler(ch)
        
        self.logger = logger
        
        return self
        
    def _get_specific_xml_data(self, xml, document_id=None):
        """Returns the parsed XML information in a dictionary of pandas dataframes.
        
        Args:
            xml (str): Path to the xml file, relative to the game folder.
            document_id (int, optional): ID to give the XML that we are parsing; Defaults to None.
        
        Returns:
            Dictionary of pandas dataframes containing XML data.
        
        """
        # Save away the values locally
        logger = self.logger
        game_url = self.game_url
        
        # Full path to XML
        xml_path = '%s%s'%(game_url, xml)
        logger.debug('XML Path: %s'%(xml_path))
        
        # Parse the XML and return the result
        xps = xp.XMLParser(xml_path, document_id=document_id)
        xps.xml_parser()
        
        return xps.dict_structure
        
    def _prep_game_xml(self):
        """Preps the game.xml file.
        
        This information can be had before the start of the game.
        Even for live games, this information will be available before game start.
        
        """
        # Save away values needed locally
        game_date = self.game_date
        game_number = self.game_number
        game_xml = self.game_configs['xml_path']
        column_configs = self.game_configs['columns_to_keep']
        
        # Download the XML
        raw_dict_game = self._get_specific_xml_data(game_xml)
        
        # Save away the variables needed
        df_game = raw_dict_game['game'].loc[:, column_configs['game']]
        df_stadium = raw_dict_game['stadium'].loc[:, column_configs['stadium']]
        df_team = raw_dict_game['team'].loc[:, column_configs['team']]
        
        # Split out the location into city & state
        df_stadium.loc[:, 'city'] = df_stadium.apply(lambda x: ', '.join(x['location'].split(', ')[:-1]), axis=1)
        df_stadium.loc[:, 'state'] = df_stadium.apply(lambda x: x['location'].split(', ')[-1], axis=1)
        df_stadium.drop(labels=['location'], axis=1, inplace=True)
        
        # Index on the team should be the type field...
        df_team.set_index(keys=['type'], inplace=True, verify_integrity=True)
        
        # Change game times into date fields
        df_game.loc[:, 'game_time_et'] = df_game.apply(lambda x: cf.convert_to_date('%s %s'%(game_date.strftime('%Y-%m-%d'), x['game_time_et'])), axis=1)
        df_game.loc[:, 'local_game_time'] = df_game.apply(lambda x: cf.convert_to_date('%s %s'%(game_date.strftime('%Y-%m-%d'), x['local_game_time'])), axis=1)
        
        # Add in a flag for whether a day or night game. Arbitrarily Uses 17 as the cut-off.
        df_game.loc[:, 'day_game_flg'] = df_game.apply(lambda x: int(x['local_game_time'].hour < 17), axis=1)
        
        # Add some additional columns to the game dataframe
        df_game.loc[:, 'game_date'] = game_date        
        df_game.loc[:, 'game_number'] = game_number
        df_game.loc[:, 'stadium_id'] = df_stadium.loc[0, 'id']
        df_game.loc[:, 'stadium_name'] = df_stadium.loc[0, 'name']
        df_game.loc[:, 'stadium_city'] = df_stadium.loc[0, 'city']
        df_game.loc[:, 'stadium_state'] = df_stadium.loc[0, 'state']
        df_game.loc[:, 'stadium_loc'] = df_stadium.loc[0, 'venue_w_chan_loc']
        df_game.loc[:, 'home_team_id'] = df_team.loc['home', 'id']
        df_game.loc[:, 'home_team_name'] = df_team.loc['home', 'name_full']
        df_game.loc[:, 'home_team_name_brief'] = df_team.loc['home', 'name_brief']
        df_game.loc[:, 'home_team_abbrev'] = df_team.loc['home', 'abbrev']
        df_game.loc[:, 'home_team_league'] = df_team.loc['home', 'league']
        df_game.loc[:, 'away_team_id'] = df_team.loc['away', 'id']
        df_game.loc[:, 'away_team_name'] = df_team.loc['away', 'name_full']
        df_game.loc[:, 'away_team_name_brief'] = df_team.loc['away', 'name_brief']
        df_game.loc[:, 'away_team_abbrev'] = df_team.loc['away', 'abbrev']
        df_game.loc[:, 'away_team_league'] = df_team.loc['away', 'league']
        
        # Reset the indices...
        df_team.reset_index(inplace=True)
        df_stadium.reset_index(inplace=True)
        
        # Game PK -- saved away to be added to all tables later
        game_pk = cf.convert_to_int(df_game.loc[0, 'game_pk'])
        
        # Game type
        self.game_type = df_game.loc[0, 'type']
        
        # Save back to the instance
        self.game_pk = game_pk
        self.df_game = df_game
        self.df_team = df_team
        self.df_stadium = df_stadium
        
        return self
        
    def _prep_players_xml(self):
        """Preps the players.xml file.
        
        This information can be had before the start of the game.
        Even for live games, this information will be available before game start.
        
        This also assumes that the 'prep_game_xml' function has already run.
        """
        def get_id(df, logger=None, **kwargs):
            """Grabs the ID that meets certain criteria.
            """
            import numpy as np
            import sys
            
            value = np.NaN
            conditions = ' & '.join(["(pd.notnull(df['%s'])) & (df['%s'] == '%s')"%(k, k, kwargs[k]) for k in kwargs.keys()])
            
            try:
                value = df.loc[eval(conditions), 'id'].iloc[0]
            except Exception:
                sys.exc_clear()
            
            return value
            
        def remove_df_dups(df, key_col):
            """Removes duplications from the dataframe
            
            Args:
                df (pandas df): Dataframe
                key_col (str): Column to use as primary key
            
            Returns:
                Un-duped Pandas dataframe
            
            """
            import numpy as np
            
            # Remove any dups
            df_dups = df.groupby([key_col]).agg({key_col: {'count': np.count_nonzero}})
            df_dups.columns = df_dups.columns.droplevel(0)
            df_dups.reset_index(drop=False, inplace=True)
            dup_ids = list(df_dups.loc[df_dups['count'] > 1, key_col])
            df = df.loc[-df[key_col].isin(dup_ids), :]
            
            return df
            
        # Save away values needed locally
        logger = self.logger
        players_xml = self.players_configs['xml_path']
        column_configs = self.players_configs['columns_to_keep']
        df_team_inst = self.df_team
        df_game = self.df_game
        
        # Download the XML
        raw_dict_players = self._get_specific_xml_data(players_xml)
        
        # Save away the variables needed
        df_team = raw_dict_players['team'].loc[:, column_configs['team']]        
        df_coach = raw_dict_players['coach'].loc[:, column_configs['coach']]
        df_umpire = raw_dict_players['umpire'].loc[:, column_configs['umpire']]
        
        # Players        
        if 'team_id_xml' in list(raw_dict_players['player'].columns):
            df_player = raw_dict_players['player'].loc[:, column_configs['player']]
            
        else:
            # We need to join back to the team
            df_player = pd.merge(raw_dict_players['player'], df_team, on=['team_id'], how='left')            
            df_player = pd.merge(df_player, df_team_inst.loc[:, ['type', 'id']], 
                                 on=['type'], how='left', suffixes=['', '_team'])
            
            # Change column names and drop unneeded
            df_player.rename(columns={'id_team': 'team_id_xml'}, inplace=True)
            df_player = df_player.loc[:, column_configs['player']]
        
        # Rename a column in df_player
        df_player.rename(columns={'team_id_xml': 'team_id'}, inplace=True)
        
        # Team and Coach information need to go together -- Let's join
        df_coach = pd.merge(df_coach, df_team, on=['team_id'], how='left')
        df_coach.drop('team_id', inplace=True, axis=1)
        
        # Grab the team id
        df_coach = pd.merge(df_coach, df_team_inst.loc[:, ['type', 'id']], 
                            on=['type'], how='left', suffixes=['', '_team'])
        
        # Change column names and drop unneeded
        df_coach.rename(columns={'id_team': 'team_id'}, inplace=True)
        df_coach.drop('type', inplace=True, axis=1)
        
        # Remove any players, coaches, umpires without name
        df_player = df_player.loc[(pd.notnull(df_player['first'])) & (df_player['first'] != ''), :]
        df_coach = df_coach.loc[(pd.notnull(df_coach['first'])) & (df_coach['first'] != ''), :]
        df_umpire = df_umpire.loc[(pd.notnull(df_umpire['name'])) & (df_umpire['name'] != ''), :]
        
        # Remove any dups
        df_player = remove_df_dups(df_player, 'id')
        df_coach = remove_df_dups(df_coach, 'id')
        df_umpire = remove_df_dups(df_umpire, 'id')
        
        # Let's grab the starting lineup and add to the dataframe for game
        # Aggregate the game information
        home_team_id = df_game.loc[:, 'home_team_id'].iloc[0]
        away_team_id = df_game.loc[:, 'away_team_id'].iloc[0]
        
        # Starting Lineup
        df_game.loc[:, 'home_1_id'] = get_id(df_player, logger, **{'team_id': home_team_id, 'bat_order': 1})
        df_game.loc[:, 'home_2_id'] = get_id(df_player, logger, **{'team_id': home_team_id, 'bat_order': 2})
        df_game.loc[:, 'home_3_id'] = get_id(df_player, logger, **{'team_id': home_team_id, 'bat_order': 3})
        df_game.loc[:, 'home_4_id'] = get_id(df_player, logger, **{'team_id': home_team_id, 'bat_order': 4})
        df_game.loc[:, 'home_5_id'] = get_id(df_player, logger, **{'team_id': home_team_id, 'bat_order': 5})
        df_game.loc[:, 'home_6_id'] = get_id(df_player, logger, **{'team_id': home_team_id, 'bat_order': 6})
        df_game.loc[:, 'home_7_id'] = get_id(df_player, logger, **{'team_id': home_team_id, 'bat_order': 7})
        df_game.loc[:, 'home_8_id'] = get_id(df_player, logger, **{'team_id': home_team_id, 'bat_order': 8})
        df_game.loc[:, 'home_9_id'] = get_id(df_player, logger, **{'team_id': home_team_id, 'bat_order': 9})
        df_game.loc[:, 'home_p_id'] = get_id(df_player, logger, **{'team_id': home_team_id, 'game_position': 'P'})
        df_game.loc[:, 'home_c_id'] = get_id(df_player, logger, **{'team_id': home_team_id, 'game_position': 'C'})
        df_game.loc[:, 'home_1b_id'] = get_id(df_player, logger, **{'team_id': home_team_id, 'game_position': '1B'})
        df_game.loc[:, 'home_2b_id'] = get_id(df_player, logger, **{'team_id': home_team_id, 'game_position': '2B'})
        df_game.loc[:, 'home_3b_id'] = get_id(df_player, logger, **{'team_id': home_team_id, 'game_position': '3B'})
        df_game.loc[:, 'home_ss_id'] = get_id(df_player, logger, **{'team_id': home_team_id, 'game_position': 'SS'})
        df_game.loc[:, 'home_lf_id'] = get_id(df_player, logger, **{'team_id': home_team_id, 'game_position': 'LF'})
        df_game.loc[:, 'home_cf_id'] = get_id(df_player, logger, **{'team_id': home_team_id, 'game_position': 'CF'})
        df_game.loc[:, 'home_rf_id'] = get_id(df_player, logger, **{'team_id': home_team_id, 'game_position': 'RF'})
        df_game.loc[:, 'home_dh_id'] = get_id(df_player, logger, **{'team_id': home_team_id, 'game_position': 'DH'})
        
        df_game.loc[:, 'away_1_id'] = get_id(df_player, logger, **{'team_id': away_team_id, 'bat_order': 1})
        df_game.loc[:, 'away_2_id'] = get_id(df_player, logger, **{'team_id': away_team_id, 'bat_order': 2})
        df_game.loc[:, 'away_3_id'] = get_id(df_player, logger, **{'team_id': away_team_id, 'bat_order': 3})
        df_game.loc[:, 'away_4_id'] = get_id(df_player, logger, **{'team_id': away_team_id, 'bat_order': 4})
        df_game.loc[:, 'away_5_id'] = get_id(df_player, logger, **{'team_id': away_team_id, 'bat_order': 5})
        df_game.loc[:, 'away_6_id'] = get_id(df_player, logger, **{'team_id': away_team_id, 'bat_order': 6})
        df_game.loc[:, 'away_7_id'] = get_id(df_player, logger, **{'team_id': away_team_id, 'bat_order': 7})
        df_game.loc[:, 'away_8_id'] = get_id(df_player, logger, **{'team_id': away_team_id, 'bat_order': 8})
        df_game.loc[:, 'away_9_id'] = get_id(df_player, logger, **{'team_id': away_team_id, 'bat_order': 9})
        df_game.loc[:, 'away_p_id'] = get_id(df_player, logger, **{'team_id': away_team_id, 'game_position': 'P'})
        df_game.loc[:, 'away_c_id'] = get_id(df_player, logger, **{'team_id': away_team_id, 'game_position': 'C'})
        df_game.loc[:, 'away_1b_id'] = get_id(df_player, logger, **{'team_id': away_team_id, 'game_position': '1B'})
        df_game.loc[:, 'away_2b_id'] = get_id(df_player, logger, **{'team_id': away_team_id, 'game_position': '2B'})
        df_game.loc[:, 'away_3b_id'] = get_id(df_player, logger, **{'team_id': away_team_id, 'game_position': '3B'})
        df_game.loc[:, 'away_ss_id'] = get_id(df_player, logger, **{'team_id': away_team_id, 'game_position': 'SS'})
        df_game.loc[:, 'away_lf_id'] = get_id(df_player, logger, **{'team_id': away_team_id, 'game_position': 'LF'})
        df_game.loc[:, 'away_cf_id'] = get_id(df_player, logger, **{'team_id': away_team_id, 'game_position': 'CF'})
        df_game.loc[:, 'away_rf_id'] = get_id(df_player, logger, **{'team_id': away_team_id, 'game_position': 'RF'})
        df_game.loc[:, 'away_dh_id'] = get_id(df_player, logger, **{'team_id': away_team_id, 'game_position': 'DH'})
        
        # Managers
        home_mgr_id = cf.coalesce(get_id(df_coach, logger, **{'team_id': home_team_id, 'position': 'manager'}), 
                                  get_id(df_coach, logger, **{'team_id': home_team_id, 'position': 'interim_manager'}), 
                                  np.NaN
                                  )
        away_mgr_id = cf.coalesce(get_id(df_coach, logger, **{'team_id': away_team_id, 'position': 'manager'}), 
                                  get_id(df_coach, logger, **{'team_id': away_team_id, 'position': 'interim_manager'}), 
                                  np.NaN
                                  )
        df_game.loc[:, 'home_mgr_id'] = home_mgr_id
        df_game.loc[:, 'away_mgr_id'] = away_mgr_id
        
        # Umpires
        df_game.loc[:, 'ump_hp_id'] = get_id(df_umpire, logger, position='home')
        df_game.loc[:, 'ump_1b_id'] = get_id(df_umpire, logger, position='first')
        df_game.loc[:, 'ump_2b_id'] = get_id(df_umpire, logger, position='second')
        df_game.loc[:, 'ump_3b_id'] = get_id(df_umpire, logger, position='third')
        df_game.loc[:, 'ump_lf_id'] = get_id(df_umpire, logger, position='left')
        df_game.loc[:, 'ump_rf_id'] = get_id(df_umpire, logger, position='right')
        
        # Save back to the instance
        self.df_player = df_player
        self.df_coach = df_coach
        self.df_umpire = df_umpire
        
        return self
        
    def _prep_boxscore_xml(self):
        """Preps the boxscore dataframes.
        """
        # Function to get flags based on note...
        def get_flag_from_note(note, val, col_val):
            """Returns 'true' if the string exists in the note
            
            Args:
                note (str): Note to search in
                val (str): Value to search for, whole word
                col_val (str): Current value in the column
            
            Returns:
                Flag value
            
            """
            import pandas as pd
            import re
            
            # Value to return
            value = col_val
            if pd.isnull(value) and pd.notnull(note):                
                pattern = '\\b%s\\b'%(val)
                search = re.search(pattern, note)
                        
                if search is not None:
                    value = 'true'
            
            return value
                    
        # Save away the values needed locally
        game_pk = self.game_pk
        boxscore_xml = self.boxscore_configs['xml_path']
        boxscore_alt_xml = self.boxscore_configs['xml_path_alt']
        column_configs = self.boxscore_configs['columns_to_keep']
                
        # Download the XML
        try:
            raw_dict_boxscore = self._get_specific_xml_data(boxscore_xml)
        except IOError:
            raw_dict_boxscore = self._get_specific_xml_data(boxscore_alt_xml)
            
            # Save away the items needed
            df_boxscore = raw_dict_boxscore['boxscore'].loc[:, column_configs['boxscore']]
            df_pitcher = raw_dict_boxscore['pitcher'].loc[:, column_configs['pitcher']]
            df_batter = raw_dict_boxscore['batter'].loc[:, column_configs['batter']]
            
            # A few columns...
            df_batter.loc[:, 'bat_order'] = df_batter.loc[:, 'bo']
            df_pitcher.loc[:, 'win'] = df_pitcher.apply(lambda x: get_flag_from_note(x['note'], 'W', x['win']), axis=1)
            df_pitcher.loc[:, 'save'] = df_pitcher.apply(lambda x: get_flag_from_note(x['note'], 'S', x['save']), axis=1)
            df_pitcher.loc[:, 'hold'] = df_pitcher.apply(lambda x: get_flag_from_note(x['note'], 'H', x['hold']), axis=1)
            df_pitcher.loc[:, 'loss'] = df_pitcher.apply(lambda x: get_flag_from_note(x['note'], 'L', x['loss']), axis=1)
            df_pitcher.loc[:, 'blown_save'] = df_pitcher.apply(lambda x: get_flag_from_note(x['note'], 'BS', x['blown_save']), axis=1)
            
            # Add in the game information
            game_info = raw_dict_boxscore['game_info'].loc[0, 'text_value']
            game_info_dict = dict((x[0], x[-1]) for x in [x.replace('<b>', '').replace('</b>', '').split(': ') for x in game_info.split('.<br/>')])
            
            # Save it to the dataframe...            
            df_boxscore.loc[:, 'attendance'] = cf.dict_value(game_info_dict, 'Att', np.NaN)
            df_boxscore.loc[:, 'elapsed_time'] = cf.dict_value(game_info_dict, 'T', np.NaN)
            df_boxscore.loc[:, 'weather'] = cf.dict_value(game_info_dict, 'Weather', np.NaN)
            df_boxscore.loc[:, 'wind'] = cf.dict_value(game_info_dict, 'Wind', np.NaN)
            
        else:
            # Save away the items needed
            df_boxscore = raw_dict_boxscore['boxscore'].loc[:, column_configs['boxscore']]
            df_pitcher = raw_dict_boxscore['pitcher'].loc[:, column_configs['pitcher']]
            df_batter = raw_dict_boxscore['batter'].loc[:, column_configs['batter']]
            
        # Split out some values in the box score and rename some columns
        df_boxscore.rename(columns={'status_ind': 'game_status'}, inplace=True)
        df_boxscore.loc[:, 'final_attendance'] = df_boxscore.loc[:, 'attendance'].apply(lambda x: cf.convert_to_int(x.replace(',', '')) if pd.notnull(x) else x)
        df_boxscore.loc[:, 'elapsed_time_minutes'] = df_boxscore.loc[:, 'elapsed_time'].apply(lambda x: cf.convert_to_int(x.split(':')[0]) * 60 + cf.convert_to_int(x.split(':')[1][:2]) if pd.notnull(x) and ':' in x else x)
        df_boxscore.loc[:, 'weather_degrees'] = df_boxscore.loc[:, 'weather'].apply(lambda x: cf.convert_to_int(x.replace(',', '').split()[x.replace(',', '').split().index('degrees') - 1]) if pd.notnull(x) and 'degrees' in x else x)
        df_boxscore.loc[:, 'weather_des'] = df_boxscore.loc[:, 'weather'].apply(lambda x: x.split(',')[-1].strip() if pd.notnull(x) else x)
        df_boxscore.loc[:, 'wind_mph'] = df_boxscore.loc[:, 'wind'].apply(lambda x: cf.convert_to_int(x.replace(',', '').split()[x.replace(',', '').split().index('mph') - 1]) if pd.notnull(x) and 'mph' in x else x)
        df_boxscore.loc[:, 'wind_direction'] = df_boxscore.loc[:, 'wind'].apply(lambda x: x.split(',')[-1].strip() if pd.notnull(x) else x)
        
        # Drop unneeded columns
        if len(df_batter) > 0:
            df_batter.drop(labels=['bo'], axis=1, inplace=True)
        
        if len(df_pitcher) > 0:
            df_pitcher.drop(labels=['note'], axis=1, inplace=True)
            
        # Combine together the dataframes
        df_players = pd.merge(df_batter, df_pitcher, on='id', how='left', suffixes=['_bat', '_pit'])
        
        # Merge to get the team data and starting order / positions
        df_players = pd.merge(self.df_player.loc[:, [
                                'id', 'team_id', 'num', 'first', 'last', 'boxname', 
                                'rl', 'bats', 'status', 'bat_order', 'game_position', 
                              ]], df_players, 
                              on='id', how='left', suffixes=['_start', ''])
                              
        assert len(self.df_player) == len(df_players), 'There were duplicate records in df_players data.'
        
        # Flags for game played & started
        df_players.loc[:, 'game_played_flg'] = df_players.apply(lambda x: int(pd.notnull(x['pos'])), axis=1)
        df_players.loc[:, 'game_started_flg'] = df_players.apply(lambda x: int(pd.notnull(x['game_position'])), axis=1)
            
        # Game Status        
        game_status = df_boxscore.loc[0, 'game_status']
        final_flg = (game_status in ['F', 'FT', 'FR', 'FO'])
        
        # Make game_pk into integer
        df_boxscore.loc[:, 'game_pk'] = df_boxscore.loc[:, 'game_pk'].apply(cf.convert_to_int)
        
        # Set the game_pk if it is null
        if pd.isnull(game_pk):
            game_pk = df_boxscore.loc[0, 'game_pk']
            self.game_pk = game_pk
        
        # Save back to the instance
        self.game_status = game_status
        self.final_flg = final_flg
        self.df_boxscore = df_boxscore
        self.df_pitcher = df_pitcher
        self.df_batter = df_batter
        self.df_players = df_players
        
        return self
        
    def _get_replaced_player(self, new_player_id, df, order='bat_order', logger=None):
        """Uses the boxscore bat order to derive the replaced player.
        
        Args:
            new_player_id (int): Player ID of the replacing player                
            df (pandas dataframe): Players (from boxscore) dataframe
            order (str): Order to use; can be either bat order or pitch order
        
        Returns:
            Integer signifying the replaced player ID.
            
        """
        import numpy as np 
        
        # Check for the correct order value
        assert order in ['bat_order', 'pitch_order'], 'Unrecognized order: %s'%(order)
        
        # Old player ID
        old_player_id = np.NaN
        
        if pd.notnull(new_player_id):
            # Team ID & Old PLayer order
            try:
                team_id = df.loc[df['id'] == new_player_id, 'team_id'].iloc[0]
                old_player_order = int(df.loc[df['id'] == new_player_id, order].iloc[0]) - 1
                # Get the Player ID
                old_player_id = df.loc[(df['team_id'] == team_id) & (df[order] == '%d'%(old_player_order)), 'id'].iloc[0]                
            except Exception:
                sys.exc_clear()
        
        return old_player_id
        
    def _add_additional_info(self, df_events, df_pitch, df_runner):
        """Adds additional information to events and pitch, including:
            # Runner Positions
            # Fielder Positions
            # Batter Bat Order
            # Batter Position
        
        Args:
            df_events: Event dataframe
            df_pitch: Pitch dataframe
            df_runner: Runner dataframe
        
        Returns:
            Two pandas dataframes: df_events, df_pitch with the additional columns
        """
        # Save away values needed locally
        logger = self.logger
        RUNNER_DEFAULT = {'1b': np.NaN, '2b': np.NaN, '3b': np.NaN}
        
        # Defensive Starting Lineup
        home_fielders = {
            'p': self.df_game.loc[:, 'home_p_id'].iloc[0], 
            'c': self.df_game.loc[:, 'home_c_id'].iloc[0], 
            '1b': self.df_game.loc[:, 'home_1b_id'].iloc[0], 
            '2b': self.df_game.loc[:, 'home_2b_id'].iloc[0], 
            '3b': self.df_game.loc[:, 'home_3b_id'].iloc[0], 
            'ss': self.df_game.loc[:, 'home_ss_id'].iloc[0], 
            'lf': self.df_game.loc[:, 'home_lf_id'].iloc[0], 
            'cf': self.df_game.loc[:, 'home_cf_id'].iloc[0], 
            'rf': self.df_game.loc[:, 'home_rf_id'].iloc[0], 
            'dh': self.df_game.loc[:, 'home_dh_id'].iloc[0], 
        }
        away_fielders = {
            'p': self.df_game.loc[:, 'away_p_id'].iloc[0], 
            'c': self.df_game.loc[:, 'away_c_id'].iloc[0], 
            '1b': self.df_game.loc[:, 'away_1b_id'].iloc[0], 
            '2b': self.df_game.loc[:, 'away_2b_id'].iloc[0], 
            '3b': self.df_game.loc[:, 'away_3b_id'].iloc[0], 
            'ss': self.df_game.loc[:, 'away_ss_id'].iloc[0], 
            'lf': self.df_game.loc[:, 'away_lf_id'].iloc[0], 
            'cf': self.df_game.loc[:, 'away_cf_id'].iloc[0], 
            'rf': self.df_game.loc[:, 'away_rf_id'].iloc[0], 
            'dh': self.df_game.loc[:, 'away_dh_id'].iloc[0], 
        }
        
        # Position tracker for df_pitch -- for more efficient loop
        start_pitch_pos = 0
        last_pitch_pos = 0
        
        # Variables to keep track of since every half-inning requires a reset
        inning_old = 0
        inning_type_old = 'T'
        
        # Dictionary to keep track of positions
        runner_dict = RUNNER_DEFAULT.copy()
        
        # Start loop with the events
        for ix_event in df_events.index:
            # Let's grab the inning & inning type
            inning = df_events.loc[ix_event, 'inning']
            inning_type = df_events.loc[ix_event, 'inning_type']
            
            # Columns of interest
            time_stamp = df_events.loc[ix_event, 'tfs_zulu']
            des = df_events.loc[ix_event, 'des']
            player_id = df_events.loc[ix_event, 'player_id']
            atbat_flg = pd.notnull(df_events.loc[ix_event, 'atbat_num'])
            off_sub_flg = (df_events.loc[ix_event, 'event'] == 'Offensive sub')
            def_sub_flg = (df_events.loc[ix_event, 'event'] in ['Pitching Substitution', 'Defensive Switch', 'Defensive Sub'])
            
            # If either does not match, we have a new half-inning that began.            
            if inning != inning_old or inning_type != inning_type_old:
                # This means we need to reset the runner positions.
                runner_dict = RUNNER_DEFAULT.copy()
                
                # Update the old values
                inning_old = inning
                inning_type_old = inning_type
                
            # If it is a defensive sub, let's update the dictionary and get the replaced_id.
            # Also, compare timestamps with df_pitch and update if the sub happened before past pitches.
            if def_sub_flg:                
                if 'Pitching Change:' in des or 'Pitcher Change:' in des:
                    replaced_position = 'p'
                elif 'to designated hitter' in des or 'as the designated hitter' in des or 'playing designated hitter' in des:
                    replaced_position = 'dh'
                elif 'to catcher' in des or 'as the catcher' in des or 'playing catcher' in des:
                    replaced_position = 'c'
                elif 'to first base' in des or 'as the first baseman' in des or 'playing first base' in des:
                    replaced_position = '1b'
                elif 'to second base' in des or 'as the second baseman' in des or 'playing second base' in des:
                    replaced_position = '2b'
                elif 'to third base' in des or 'as the third baseman' in des or 'playing third base' in des:
                    replaced_position = '3b'
                elif 'to shortstop' in des or 'as the shortstop' in des or 'playing shortstop' in des:
                    replaced_position = 'ss'
                elif 'to left field' in des or 'as the left fielder' in des or 'playing left field' in des:
                    replaced_position = 'lf'
                elif 'to center field' in des or 'as the center fielder' in des or 'playing center field' in des:
                    replaced_position = 'cf'
                elif 'to right field' in des or 'as the right fielder' in des or 'playing right field' in des:
                    replaced_position = 'rf'
                else:
                    raise ValueError('Unrecognized description in a sub event.')
                
                # Grab the timestamp from the last pitch.
                last_pitch_timestamp = df_pitch.loc[last_pitch_pos, 'tfs_zulu']
                
                # If this last pitch timestamp is greater than the timestamp of the sub, we need to update with the sub information
                if pd.notnull(last_pitch_timestamp) and pd.notnull(time_stamp) and time_stamp < last_pitch_timestamp:
                    counter = last_pitch_pos
                    while pd.notnull(last_pitch_timestamp) and last_pitch_timestamp > time_stamp:
                        df_pitch.loc[counter, 'fielder_%s'%(replaced_position)] = player_id
                        
                        # Decrease the counter and update timestamp
                        counter -= 1
                        last_pitch_timestamp = df_pitch.loc[counter, 'tfs_zulu']
                
                # Get the replaced_id and
                # Update the fielder dictionary with the sub that happened
                if inning_type == 'T':
                    # Top means home team is fielding
                    df_events.loc[ix_event, 'replaced_id'] = home_fielders[replaced_position]
                    home_fielders[replaced_position] = player_id
                elif inning_type == 'B':
                    # This means that the home team is batting - thus use the away lineup
                    df_events.loc[ix_event, 'replaced_id'] = away_fielders[replaced_position]
                    away_fielders[replaced_position] = player_id
            
            # If it is a offensive sub, let's update the dictionary and get the replaced_id through the bat_order field.
            # Also, compare timestamps with df_pitch and update if the sub happened before past pitches.
            if off_sub_flg:
                # Get the replaced_id from the bat_order
                replaced_player_id = self._get_replaced_player(player_id, self.df_players, logger=logger)
                df_events.loc[ix_event, 'replaced_id'] = replaced_player_id
                
                # If it is a pinch hitter, we may need to update the batter_id's at the pitch level.
                if 'Pinch-hitter' in des and pd.notnull(replaced_player_id):
                    # Grab the timestamp from the last pitch.
                    last_pitch_timestamp = df_pitch.loc[last_pitch_pos, 'tfs_zulu']
                    
                    # If this last pitch timestamp is greater than the timestamp of the sub, we need to update with the sub information
                    if pd.notnull(last_pitch_timestamp) and pd.notnull(time_stamp) and time_stamp < last_pitch_timestamp:
                        # Go through the pitch of the previous atbat.
                        for pitch_pos in xrange(start_pitch_pos, last_pitch_pos + 1):
                            pitch_timestamp = df_pitch.loc[pitch_pos, 'tfs_zulu']
                            
                            if pd.notnull(pitch_timestamp) and pitch_timestamp < time_stamp:
                                start_pitch_pos = pitch_pos + 1
                                df_pitch.loc[pitch_pos, 'batter_id'] = replaced_player_id
                            elif pd.notnull(pitch_timestamp) and pitch_timestamp >= time_stamp:
                                df_pitch.loc[pitch_pos, 'batter_id'] = player_id
            
            # If it is an atbat event, let's update the columns needed.
            if atbat_flg:
                # Grab the atbat specific information needed
                atbat_num = df_events.loc[ix_event, 'atbat_num']
                
                # Bat Order
                try:
                    df_events.loc[ix_event, 'bat_order'] = int(self.df_players.loc[(self.df_players['id'] == player_id), 'bat_order'].iloc[0][:1])
                except Exception:
                    df_events.loc[ix_event, 'bat_order'] = np.NaN
                
                # The runner starting position is as given in the dictionary.
                df_events.loc[ix_event, 'runner_1b_prev'] = runner_dict['1b']
                df_events.loc[ix_event, 'runner_2b_prev'] = runner_dict['2b']
                df_events.loc[ix_event, 'runner_3b_prev'] = runner_dict['3b']
                
                # Let's figure out where the runners moved to, if out count is not 3 (end of inning).
                if df_events.loc[ix_event, 'o'] != 3:
                    # Get all runners 
                    df_runner_temp = df_runner.loc[(df_runner['atbat_num'] == atbat_num), :]
                    for ix_runner in df_runner_temp.index:
                        runner_id = df_runner_temp.loc[ix_runner, 'player_id']
                        start_pos = df_runner_temp.loc[ix_runner, 'start'].lower()
                        end_pos = df_runner_temp.loc[ix_runner, 'end'].lower()
                        
                        if start_pos != '':
                            # The runner moved from this starting position, so we should remove.
                            runner_dict[start_pos] = np.NaN
                        
                        if end_pos != '':
                            # Update the end position with the correct runner.
                            runner_dict[end_pos] = runner_id
                            
                else:
                    # If out count is 3, we can just reset the runner dictionary.
                    runner_dict = RUNNER_DEFAULT.copy()
                    
                # Update the runner positions at the end of the atbat.
                df_events.loc[ix_event, 'runner_1b'] = runner_dict['1b']
                df_events.loc[ix_event, 'runner_2b'] = runner_dict['2b']
                df_events.loc[ix_event, 'runner_3b'] = runner_dict['3b']
                
                # The fielder starting position is as given in the dictionary.
                if inning_type == 'T':
                    # This means that the away team is batting - thus use the home lineup
                    df_events.loc[ix_event, 'fielder_p_prev'] = home_fielders['p']
                    df_events.loc[ix_event, 'fielder_c_prev'] = home_fielders['c']
                    df_events.loc[ix_event, 'fielder_1b_prev'] = home_fielders['1b']
                    df_events.loc[ix_event, 'fielder_2b_prev'] = home_fielders['2b']
                    df_events.loc[ix_event, 'fielder_3b_prev'] = home_fielders['3b']
                    df_events.loc[ix_event, 'fielder_ss_prev'] = home_fielders['ss']
                    df_events.loc[ix_event, 'fielder_lf_prev'] = home_fielders['lf']
                    df_events.loc[ix_event, 'fielder_cf_prev'] = home_fielders['cf']
                    df_events.loc[ix_event, 'fielder_rf_prev'] = home_fielders['rf']
                    
                    # Batter Position
                    try:
                        df_events.loc[ix_event, 'batter_position'] = (key for key, value in away_fielders.items() if value == player_id).next()
                    except Exception:
                        # If no match exists, it means that the batter was a pinch-hitter
                        df_events.loc[ix_event, 'batter_position'] = 'ph'
                    
                elif inning_type == 'B':
                    # This means that the home team is batting - thus use the away lineup
                    df_events.loc[ix_event, 'fielder_p_prev'] = away_fielders['p']
                    df_events.loc[ix_event, 'fielder_c_prev'] = away_fielders['c']
                    df_events.loc[ix_event, 'fielder_1b_prev'] = away_fielders['1b']
                    df_events.loc[ix_event, 'fielder_2b_prev'] = away_fielders['2b']
                    df_events.loc[ix_event, 'fielder_3b_prev'] = away_fielders['3b']
                    df_events.loc[ix_event, 'fielder_ss_prev'] = away_fielders['ss']
                    df_events.loc[ix_event, 'fielder_lf_prev'] = away_fielders['lf']
                    df_events.loc[ix_event, 'fielder_cf_prev'] = away_fielders['cf']
                    df_events.loc[ix_event, 'fielder_rf_prev'] = away_fielders['rf']
                    
                    # Batter Position
                    try:
                        df_events.loc[ix_event, 'batter_position'] = (key for key, value in home_fielders.items() if value == player_id).next()
                    except Exception:
                        # If no match exists, it means that the batter was a pinch-hitter
                        df_events.loc[ix_event, 'batter_position'] = 'ph'
                        
                # Now, let's update the fielders and batters at the pitch level.
                for ix_pitch in df_pitch.loc[df_pitch['atbat_num'] == atbat_num, :].index:                    
                    # Update the positions
                    if df_pitch.loc[ix_pitch, 'atbat_pitch_number'] == 1:
                        start_pitch_pos = ix_pitch
                    last_pitch_pos = ix_pitch
                    
                    # Batter ID
                    df_pitch.loc[ix_pitch, 'batter_id'] = player_id                    
                    
                    # The fielder starting position is as given in the dictionary.
                    if inning_type == 'T':
                        # This means that the away team is batting - thus use the home lineup
                        df_pitch.loc[ix_pitch, 'fielder_p'] = home_fielders['p']
                        df_pitch.loc[ix_pitch, 'fielder_c'] = home_fielders['c']
                        df_pitch.loc[ix_pitch, 'fielder_1b'] = home_fielders['1b']
                        df_pitch.loc[ix_pitch, 'fielder_2b'] = home_fielders['2b']
                        df_pitch.loc[ix_pitch, 'fielder_3b'] = home_fielders['3b']
                        df_pitch.loc[ix_pitch, 'fielder_ss'] = home_fielders['ss']
                        df_pitch.loc[ix_pitch, 'fielder_lf'] = home_fielders['lf']
                        df_pitch.loc[ix_pitch, 'fielder_cf'] = home_fielders['cf']
                        df_pitch.loc[ix_pitch, 'fielder_rf'] = home_fielders['rf']
                        
                    elif inning_type == 'B':
                        # This means that the home team is batting - thus use the away lineup
                        df_pitch.loc[ix_pitch, 'fielder_p'] = away_fielders['p']
                        df_pitch.loc[ix_pitch, 'fielder_c'] = away_fielders['c']
                        df_pitch.loc[ix_pitch, 'fielder_1b'] = away_fielders['1b']
                        df_pitch.loc[ix_pitch, 'fielder_2b'] = away_fielders['2b']
                        df_pitch.loc[ix_pitch, 'fielder_3b'] = away_fielders['3b']
                        df_pitch.loc[ix_pitch, 'fielder_ss'] = away_fielders['ss']
                        df_pitch.loc[ix_pitch, 'fielder_lf'] = away_fielders['lf']
                        df_pitch.loc[ix_pitch, 'fielder_cf'] = away_fielders['cf']
                        df_pitch.loc[ix_pitch, 'fielder_rf'] = away_fielders['rf']
                        
        return df_events, df_pitch
        
    def _prep_events_xml(self, final_flg=True):
        """Preps the event-related dataframes. 
        
        This can be done during the game, but most likely will want to be done afterwards.
        Tries to processes the relevant XML files in the 'inning' folder.
        Looks for inning_all and goes to the individual inning files if not found.
        Uses inning_hit to get the hitchart.
        
        Args:
            final_flg (bln, optional): False if the game is not final and the events are not yet needed.
                Defaults to True.
            
        """            
        # Function to derive fielded by position
        def get_fielded_by_position(des, pa_flg, logger=None):
            """Gets the position that the ball was fielded by.
            
            Args:
                des (str): Play description
                pa_flg (int): 1 if PA and function needs to be run.
            
            Returns:
                Position abbreviation (string).
            
            """
            import numpy as np
            import re
            import common_functions as cf
            
            # Value to return
            value = np.NaN
            
            # If not a plate appearance, no need to run the function
            if pa_flg != 1:
                return value
            
            # All positions
            positions = {'pitcher': 'p', 'catcher': 'c', 
                         'first baseman': '1b', 'second baseman': '2b', 
                         'third baseman': '3b', 'shortstop': 'ss', 
                         'left fielder': 'lf', 'center fielder': 'cf', 'right fielder': 'rf'}
            
            # Only need to go through this if there was an in-play pitch
            # If another in-play, we need to figure out the first fielder in the description
            pos = len(des)
            loc = None
            for p in positions.keys():
                pattern = '\\b%s\\b'%(p)
                search = re.search(pattern, des)
                
                if search is not None:
                    pos_found = search.start()
                    if pos_found < pos:
                        pos = pos_found
                        loc = p
            
            value = cf.dict_value(positions, loc, default=np.NaN)
                    
            return value
            
        # Do not go through the data pull if the final_flg given is False
        if final_flg == False:
            return self
            
        # Save away the values needed locally
        logger = self.logger
        events_xml = self.events_configs['xml_path']
        column_configs = self.events_configs['columns_to_keep']
        
        for x in events_xml:
            if x == 'inning/inning_hit.xml':
                raw_dict_hit = self._get_specific_xml_data(x)
            elif x == 'inning/inning_all.xml':
                try:
                    # First, try to see if inning_all exists.
                    raw_dict_inning = self._get_specific_xml_data(x, document_id=0)
                except IOError:
                    # If it does not exist, go for the individual inning files.
                    raw_dict_inning_list = []
                    inning = 1
                    
                    # This ensures that we get up to the last available inning, since extras are possible.
                    while True:
                        url = 'inning/inning_%d.xml'%(inning)
                        try:                            
                            raw_dict = self._get_specific_xml_data(url, document_id=inning)
                        except IOError:
                            break
                        else:
                            inning += 1
                            raw_dict_inning_list.append(raw_dict)
                    
                    if len(raw_dict_inning_list) > 0:
                        # Dictionary that will hold the appended data
                        raw_dict_inning = {}
                        key_list = []
                        
                        # All keys
                        for x in raw_dict_inning_list:
                            key_list.extend(list(x.keys()))
                            
                        for k in list(set(key_list)):
                            df = pd.concat([cf.dict_value(raw_dict_inning_list[x], k) for x in xrange(len(raw_dict_inning_list))])
                            df.reset_index(inplace=True, drop=True)
                            raw_dict_inning.update({k: df})
                    else:
                        raw_dict_inning = {}
            else:
                print '%s not recognized as a valid XML file.'%(x)
                
        # Hitchart processing...
        df_hitchart = raw_dict_hit['hip'].loc[:, column_configs['hip']]
        df_hitchart.loc[:, 'inning'] = df_hitchart.loc[:, 'inning'].apply(cf.convert_to_int)
        
        # Transform the x, y coordinates according to MLBAM specs
        # https://github.com/beanumber/openWAR/blob/master/R/GameDay.R
        df_hitchart.loc[:, 'ballpark_x'] = df_hitchart.apply(lambda x: float(x['x']) - 125.0, axis=1)
        df_hitchart.loc[:, 'ballpark_y'] = df_hitchart.apply(lambda x: 199.0 - float(x['y']), axis=1)
        df_hitchart.loc[:, 'r'] = df_hitchart.apply(lambda x: (np.sqrt(90 ** 2 + 90 ** 2) / 51) * np.sqrt(x['ballpark_x'] ** 2 + x['ballpark_y'] ** 2), axis=1)
        df_hitchart.loc[:, 'theta'] = df_hitchart.apply(lambda x: np.arctan2(x['ballpark_y'], x['ballpark_x']), axis=1)
        df_hitchart.loc[:, 'ballpark_x'] = df_hitchart.apply(lambda x: x['r'] * np.cos(x['theta']), axis=1)
        df_hitchart.loc[:, 'ballpark_y'] = df_hitchart.apply(lambda x: x['r'] * np.sin(x['theta']), axis=1)
        
        # Rename columns for hit chart
        df_hitchart.rename(columns={'des': 'event', 'batter': 'player_id', 'pitcher': 'pitcher_id'}, inplace=True)
        
        # Drop records that have x = 1 and y = 1 as these aren't real hitchart records.
        df_hitchart = df_hitchart[(df_hitchart['x'] != '1') | (df_hitchart['y'] != '1')]
        
        # Actions
        df_action = raw_dict_inning['action']
        df_action_columns = list(df_action.columns)
        df_action_columns.extend(['top_id', 'bottom_id'])
        
        df_action = df_action.loc[:, list(np.unique(df_action_columns))]
        df_action.loc[:, 'inning_id'] = None
        df_action.loc[:, 'inning_type'] = ''
        
        df_action = pd.merge(df_action, raw_dict_inning['top'], on=['xml_document_id', 'top_id'], how='left', suffixes=['', '_t'])
        df_action = pd.merge(df_action, raw_dict_inning['bottom'], on=['xml_document_id', 'bottom_id'], how='left', suffixes=['', '_b'])
        
        df_action.loc[pd.notnull(df_action.loc[:, 'top_id']), 'inning_type'] = 'T'
        df_action.loc[pd.notnull(df_action.loc[:, 'bottom_id']), 'inning_type'] = 'B'
        df_action.loc[:, 'inning_id'] = df_action.apply(lambda x: cf.coalesce(x['inning_id_t'], x['inning_id_b']), axis=1)
        df_action = pd.merge(df_action, raw_dict_inning['inning'].loc[:, ['xml_document_id', 'inning_id', 'num']], on=['xml_document_id', 'inning_id'], how='left')
        df_action = df_action.loc[:, column_configs['action']]
        df_action.rename(columns={'num': 'inning', 'player': 'player_id'}, inplace=True)
        df_action.loc[:, 'inning'] = df_action.loc[:, 'inning'].apply(cf.convert_to_int)
        
        # Atbats
        df_atbat = raw_dict_inning['atbat']
        df_atbat_columns = list(df_atbat.columns)
        df_atbat_columns.extend(['top_id', 'bottom_id'])
        
        df_atbat = df_atbat.loc[:, list(np.unique(df_atbat_columns))]
        df_atbat.loc[:, 'inning_id'] = None
        df_atbat.loc[:, 'inning_type'] = ''
        
        df_atbat = pd.merge(df_atbat, raw_dict_inning['top'], on=['xml_document_id', 'top_id'], how='left', suffixes=['', '_t'])
        df_atbat = pd.merge(df_atbat, raw_dict_inning['bottom'], on=['xml_document_id', 'bottom_id'], how='left', suffixes=['', '_b'])
        
        df_atbat.loc[pd.notnull(df_atbat.loc[:, 'top_id']), 'inning_type'] = 'T'
        df_atbat.loc[pd.notnull(df_atbat.loc[:, 'bottom_id']), 'inning_type'] = 'B'
        df_atbat.loc[:, 'inning_id'] = df_atbat.apply(lambda x: cf.coalesce(x['inning_id_t'], x['inning_id_b']), axis=1)
        df_atbat = pd.merge(df_atbat, raw_dict_inning['inning'].loc[:, ['xml_document_id', 'inning_id', 'num']], on=['xml_document_id', 'inning_id'], how='left', suffixes=['', '_i'])
        df_atbat = df_atbat.loc[:, column_configs['atbat']]
        df_atbat.rename(columns={'num_i': 'inning', 
                                 'num': 'atbat_num', 
                                 'batter': 'player_id', 
                                 'pitcher': 'pitcher_id', 
                                 'start_tfs_zulu': 'tfs_zulu', 
                                 'away_team_runs': 'away_score', 
                                 'home_team_runs': 'home_score', 
                                 }, inplace=True)
                                 
        # Sort by atbat number
        df_atbat.loc[:, 'inning'] = df_atbat.loc[:, 'inning'].apply(cf.convert_to_int)
        df_atbat.loc[:, 'atbat_num'] = df_atbat.loc[:, 'atbat_num'].apply(cf.convert_to_int)
        df_atbat.sort(columns=['inning', 'inning_type', 'atbat_num'], 
                       ascending=[True, False, True], inplace=True, na_position='first')
                
        # Convert relevant columns to integers
        df_atbat.loc[:, 'o'] = df_atbat.loc[:, 'o'].apply(cf.convert_to_int)
        df_atbat.loc[:, 'away_score'] = df_atbat.loc[:, 'away_score'].apply(cf.convert_to_int)
        df_atbat.loc[:, 'home_score'] = df_atbat.loc[:, 'home_score'].apply(cf.convert_to_int)
        #print df_atbat.loc[:, ['home_score', 'away_score']]
        
        # Fill forward the score to cover NA's
        df_atbat.loc[:, ['away_score', 'home_score']] = df_atbat.loc[:, ['away_score', 'home_score']].fillna(method='pad', inplace=False)
        #print df_atbat.loc[:, ['home_score', 'away_score']]
        
        # Any other NA's are in the beginning, when the score was 0.
        df_atbat.fillna(value={'away_score': 0, 'home_score': 0}, axis=0, inplace=True)
        
        # Compare to previous at-bat for outs and scores
        df_atbat.loc[:, 'o_prev'] = df_atbat.loc[:, 'o'].shift(1)
        df_atbat.loc[:, 'away_score_prev'] = df_atbat.loc[:, 'away_score'].shift(1)
        df_atbat.loc[:, 'home_score_prev'] = df_atbat.loc[:, 'home_score'].shift(1)
        df_atbat.loc[:, 'o_prev'] = df_atbat.loc[:, 'o_prev'].apply(lambda x: x if (pd.notnull(x) and int(x) >= 0 and int(x) <= 2) else 0)
        df_atbat.loc[:, 'no_outs'] = df_atbat.apply(lambda x: x['o'] - x['o_prev'], axis=1)
        df_atbat.loc[:, 'score_diff'] = df_atbat.apply(lambda x: x['home_score'] - x['away_score'], axis=1)
        
        # Fill Score NA's with 0, since that is just the beginning of the game
        df_atbat.fillna(value={'away_score_prev': 0, 'home_score_prev': 0}, axis=0, inplace=True)
        
        # Concatenate the atbat and action dataframes into "events"
        df_events = pd.concat([df_atbat, df_action], axis=0)
        
        # Count the number of records... To be compared later to ensure we have no duplicates.
        events_length = len(df_events)
        
        # Pickoffs
        if 'po' in raw_dict_inning.keys():
            df_po = pd.merge(raw_dict_inning['po'], raw_dict_inning['atbat'].loc[:, ['xml_document_id', 'atbat_id', 'num']], on=['xml_document_id', 'atbat_id'], how='left')
            df_po = df_po.loc[:, column_configs['po']]
            df_po.rename(columns={'num': 'atbat_num'}, inplace=True)
            
            df_po.loc[:, 'base'] = df_po.apply(lambda x: x['des'][-2:].lower() + '_pickoffs', axis=1)
            
            df_po_summary = pd.pivot_table(df_po, values='des', index='atbat_num', columns='base', aggfunc=len, fill_value=0)
            df_po_summary.reset_index(drop=False, inplace=True)
        else:
            df_po_summary = pd.DataFrame()
        
        # Add in the pickoff counts
        if len(df_po_summary) > 0:
            df_events = pd.merge(df_events, df_po_summary, on='atbat_num', how='left')
        
        # Cut to only the columns needed
        df_events = df_events.loc[:, column_configs['events']]
        
        # Runners
        df_runner = pd.merge(raw_dict_inning['runner'], raw_dict_inning['atbat'].loc[:, ['xml_document_id', 'atbat_id', 'num']], on=['xml_document_id', 'atbat_id'], how='left')
        df_runner = df_runner.loc[:, column_configs['runner']]
        df_runner.rename(columns={'id': 'player_id', 'num': 'atbat_num'}, inplace=True)
        
        # Add in the inning & inning type
        df_runner = pd.merge(df_runner, df_events.loc[:, ['atbat_num', 'inning', 'inning_type']], on='atbat_num', how='left')
        
        # Event number and atbat number should be integers
        df_runner.loc[:, 'atbat_num'] = df_runner.loc[:, 'atbat_num'].apply(cf.convert_to_int)
        df_runner.loc[:, 'event_num'] = df_runner.loc[:, 'event_num'].apply(cf.convert_to_int)
        
        # Let's get the last pitch information...
        df_runner_groupby = df_runner.groupby(['atbat_num'], as_index=False)
        df_runner_atbat = df_runner_groupby.agg({'score': lambda x: len(x[pd.notnull(x)]), 
                                                 'rbi': lambda x: len(x[pd.notnull(x)]), 
                                                 'earned': lambda x: len(x[pd.notnull(x)]), 
                                                })
        
        # Pitches
        df_pitch = pd.merge(raw_dict_inning['pitch'], raw_dict_inning['atbat'].loc[:, ['xml_document_id', 'atbat_id', 'num']], on=['xml_document_id', 'atbat_id'], how='left')
        df_pitch = df_pitch.loc[:, column_configs['pitch']]
        df_pitch.rename(columns={'id': 'pitch_id', 'num': 'atbat_num', 'on_1b': 'runner_1b', 'on_2b': 'runner_2b', 'on_3b': 'runner_3b'}, inplace=True)
        
        # Pitch ID, Event number, and atbat number should be integers
        df_pitch.loc[:, 'pitch_id'] = df_pitch.loc[:, 'pitch_id'].apply(cf.convert_to_int)
        df_pitch.loc[:, 'atbat_num'] = df_pitch.loc[:, 'atbat_num'].apply(cf.convert_to_int)
        df_pitch.loc[:, 'event_num'] = df_pitch.loc[:, 'event_num'].apply(cf.convert_to_int)
        
        # Merge with events to get the relevant information
        df_pitch = pd.merge(df_pitch, df_events.loc[:, ['atbat_num', 'inning', 'inning_type', 'pitcher_id']], on='atbat_num', how='left')        
        
        # Umpire decisions
        df_pitch.loc[:, 'umpire_decision_flg'] = df_pitch.apply(lambda x: int(x['des'] in ['Called Strike', 'Ball', 'Ball In Dirt']), axis=1)
        
        # TFS Zulu is a date field
        df_pitch.loc[:, 'tfs_zulu'] = df_pitch.loc[:, 'tfs_zulu'].apply(cf.convert_to_date)
        
        # Sort...
        df_pitch.sort(columns=['inning', 'inning_type', 'tag_counter'], 
                      ascending=[True, False, True], inplace=True, na_position='first')
        df_pitch.reset_index(drop=True, inplace=True)
        
        # Pitch Types
        df_pitch.loc[:, 'fastball_flg'] = df_pitch.apply(lambda x: int(x['pitch_type'] in ['FA', 'FF', 'FT']), axis=1)
        df_pitch.loc[:, 'cutter_flg'] = df_pitch.apply(lambda x: int(x['pitch_type'] == 'FC'), axis=1)
        df_pitch.loc[:, 'curve_flg'] = df_pitch.apply(lambda x: int(x['pitch_type'] in ['CU', 'KC', 'SC']), axis=1)
        df_pitch.loc[:, 'slider_flg'] = df_pitch.apply(lambda x: int(x['pitch_type'] in ['SC', 'SL']), axis=1)
        df_pitch.loc[:, 'knuckleball_flg'] = df_pitch.apply(lambda x: int(x['pitch_type'] == 'KN'), axis=1)
        df_pitch.loc[:, 'changeup_flg'] = df_pitch.apply(lambda x: int(x['pitch_type'] == 'CH'), axis=1)
        df_pitch.loc[:, 'sinker_flg'] = df_pitch.apply(lambda x: int(x['pitch_type'] == 'SI'), axis=1)
        df_pitch.loc[:, 'splitter_flg'] = df_pitch.apply(lambda x: int(x['pitch_type'] in ['FS', 'FO']), axis=1)
        
        # Pitch number per at-bat
        df_pitch.loc[:, 'atbat_pitch_number'] = np.NaN
        atbat_num_old = 0
        
        for i in df_pitch.index:
            atbat_num = df_pitch.loc[i, 'atbat_num']
            
            if atbat_num != atbat_num_old:
                counter = 1
            else:
                counter += 1
            
            df_pitch.loc[i, 'atbat_pitch_number'] = counter
            atbat_num_old = atbat_num
            
        # Add some flags to events...
        # Some aggregated flags to signify key items
        df_events.loc[:, 'pa_flg'] = df_events.apply(lambda x: int((pd.notnull(x['atbat_num'])) & (x['event'] not in ['Runner Out'])), axis=1)
        df_events.loc[:, 'ab_flg'] = df_events.apply(lambda x: int((x['pa_flg'] == 1) & (x['event'] not in ['Walk', 'Intent Walk', 'Hit By Pitch', 'Sac Fly', 'Sac Fly DP', 'Sac Bunt', 'Sacrifice Bunt DP', 'Catcher Interference'])), axis=1)
        df_events.loc[:, 'inplay_flg'] = df_events.apply(lambda x: int((x['pa_flg'] == 1) & (x['event'] not in ['Walk', 'Intent Walk', 'Hit By Pitch', 'Strikeout - DP', 'Strikeout', 'Catcher Interference'])), axis=1)
        df_events.loc[:, 'obp_calc_flg'] = df_events.apply(lambda x: int(x['event'] in ['Single', 'Double', 'Triple', 'Home Run', 'Intent Walk', 'Walk', 'Hit By Pitch']), axis=1)
        
        df_events.loc[:, 'hit_flg'] = df_events.apply(lambda x: int(x['event'] in ['Single', 'Double', 'Triple', 'Home Run']), axis=1)
        df_events.loc[:, 'reached_flg'] = df_events.apply(lambda x: int(x['event'] in ['Single', 'Double', 'Triple', 'Home Run', 'Intent Walk', 'Walk', 'Hit By Pitch', 'Catcher Interference']), axis=1)            
        df_events.loc[:, 'ob_flg'] = df_events.apply(lambda x: int(x['event'] in ['Single', 'Double', 'Triple', 'Home Run', 'Intent Walk', 'Walk', 'Hit By Pitch']), axis=1)        
        df_events.loc[:, 'hit_value'] = df_events.apply(lambda x: int(x['event'] == 'Single') + (int(x['event'] == 'Double') * 2) + (int(x['event'] == 'Triple') * 3) + (int(x['event'] == 'Home Run') * 4), axis=1)
        
        # Substitutions and switches
        df_events.loc[:, 'offensive_sub_flg'] = df_events.apply(lambda x: int(x['event'] == 'Offensive sub'), axis=1)
        df_events.loc[:, 'pitching_sub_flg'] = df_events.apply(lambda x: int(x['event'] == 'Pitching Substitution'), axis=1)
        df_events.loc[:, 'defensive_sub_flg'] = df_events.apply(lambda x: int(x['event'] in ['Defensive Switch', 'Defensive Sub']), axis=1)
        df_events.loc[:, 'sub_flg'] = df_events.apply(lambda x: int(x['event'] in['Offensive sub', 'Pitching Substitution', 'Defensive Switch', 'Defensive Sub']), axis=1)
        
        # Fielder information
        df_events.loc[:, 'fielded_by_position'] = df_events.apply(lambda x: get_fielded_by_position(x['des'], x['pa_flg'], logger), axis=1)
        
        # Event number should be integers        
        df_events.loc[:, 'event_num'] = df_events.loc[:, 'event_num'].apply(cf.convert_to_int)
        
        # TFS Zulu is a date field
        df_events.loc[:, 'tfs_zulu'] = df_events.loc[:, 'tfs_zulu'].apply(cf.convert_to_date)
                             
        # Merge with runners to get the number of RBI's, scores, and earned runs to an at-bat.
        df_events = pd.merge(df_events, 
                             df_runner_atbat.loc[:, [
                                 'atbat_num', 
                                 'score', 
                                 'rbi', 
                                 'earned', 
                                 ]], 
                             on='atbat_num', how='left')
                             
        # Fill NA's with zero for the above three columns
        df_events.fillna(value={'score': 0, 'rbi': 0, 'earned': 0}, axis=0, inplace=True)
        
        # Sort the events
        # Sort order should be: inning, inning_type, tfs_zulu (timestamp), tag counter
        df_events.sort(columns=['inning', 'inning_type', 'tfs_zulu', 'tag_counter'], 
                       ascending=[True, False, True, True], inplace=True, na_position='first')
        df_events.reset_index(drop=True, inplace=True)
        
        # Run the function to add in additional information through a loop
        df_events, df_pitch = self._add_additional_info(df_events, df_pitch, df_runner)
        
        # First, if the fielder_p field is null or 0, replace with the current pitcher_id
        df_pitch.loc[:, 'fielder_p'] = df_pitch.apply(lambda x: x['pitcher_id'] if pd.isnull(x['fielder_p']) or x['fielder_p'] == '0' else x['fielder_p'], axis=1)
        
        # Update the pitcher_id column
        df_pitch.loc[:, 'pitcher_id'] = df_pitch.loc[:, 'fielder_p']
        
        # Join the hitchart information.
        # Add in the at-bat number for every hitchart record...
        start_index = 0
        df_hitchart.loc[:, 'atbat_num'] = np.NaN
        for ix in df_hitchart.index:
            inning = df_hitchart.loc[ix, 'inning']
            pitcher_id = df_hitchart.loc[ix, 'pitcher_id']
            player_id = df_hitchart.loc[ix, 'player_id']
            
            # Now, loop through the at-bats. Find the first matching one.
            for i in xrange(start_index, len(df_events)):
                if inning == df_events.loc[:, 'inning'].iloc[i] and \
                    pitcher_id == df_events.loc[:, 'pitcher_id'].iloc[i] and \
                    player_id == df_events.loc[:, 'player_id'].iloc[i] and \
                    df_events.loc[:, 'inplay_flg'].iloc[i] == 1:
                        df_hitchart.loc[ix, 'atbat_num'] = df_events.loc[:, 'atbat_num'].iloc[i]
                        start_index = i + 1
                        break
                    
        # Issue warning if there are any null atbat records.
        #df_hitchart.dropna(axis=0, how='all', subset=['atbat_num'], inplace=True)
        if len(df_hitchart[pd.isnull(df_hitchart['atbat_num'])]) != 0:
            warnings.warn('There were hitchart records without a matching atbat in ID %s.'%(self.game_id))
            
        # Merge using the atbat number
        df_events = pd.merge(df_events, 
                             df_hitchart.loc[pd.notnull(df_hitchart['atbat_num']), [
                                 'atbat_num', 'x', 'y', 'r', 'theta', 
                                 'ballpark_x', 'ballpark_y']], 
                             how='left', on=['atbat_num'])

        # Let's get the last pitch information by at-bat.
        df_pitch_last_groupby = df_pitch.groupby(['atbat_num'], as_index=False)
        df_pitch_last = df_pitch_last_groupby.agg({'atbat_pitch_number': np.max})
        df_pitch_last = pd.merge(df_pitch_last, df_pitch, on=['atbat_num', 'atbat_pitch_number'])
        
        # Add an in play flag...
        df_pitch_last.loc[:, 'pitch_inplay_flg'] = df_pitch_last.apply(lambda x: int('In play,' in x['des']), axis=1)
        #self.df_pitch_last = df_pitch_last
        
        # Rename columns for various pitch characteristics that we want to keep
        df_pitch_last.rename(columns={
                'atbat_pitch_number': 'pitches_seen', 
                'start_speed': 'pitch_speed', 
                'end_speed': 'pitch_end_speed', 
                'px': 'pitch_horizontal_location', 
                'pz': 'pitch_vertical_location', 
            }, inplace=True)
        
        # Merge with the last pitch to get the relevant information, including:
        # inplay flag, fielder positions
        # Pitch characteristics
        df_events = pd.merge(df_events, 
                             df_pitch_last.loc[:, [
                                 'atbat_num', 
                                 'pitches_seen', 
                                 'pitch_type', 
                                 'pitch_inplay_flg', 
                                 'pitch_speed', 
                                 'pitch_end_speed', 
                                 'pitch_horizontal_location', 
                                 'pitch_vertical_location', 
                                 'fielder_p', 
                                 'fielder_c', 
                                 'fielder_1b', 
                                 'fielder_2b', 
                                 'fielder_3b', 
                                 'fielder_ss', 
                                 'fielder_lf', 
                                 'fielder_cf', 
                                 'fielder_rf', 
                                 ]], 
                             on='atbat_num', how='left')
        
        # Replace fielding pitcher with the correct ID if prev is 0.
        df_events.loc[:, 'fielder_p_prev'] = df_events.apply(lambda x: x['pitcher_id'] if pd.isnull(x['fielder_p_prev']) or x['fielder_p_prev'] == '0' else x['fielder_p_prev'], axis=1)
        
        # Grab the player who fielded based on the position
        df_events.loc[:, 'fielded_by_id'] = df_events.apply(lambda x: x['fielder_%s'%(x['fielded_by_position'])] if pd.notnull(x['fielded_by_position']) else np.NaN, axis=1)
        
        # Look for challenges and grab results
        df_events.loc[:, 'challenge_flg'] = df_events.loc[:, 'des'].apply(lambda x: int('challenged' in x.lower()))
        df_events.loc[:, 'challenge_overturned_flg'] = df_events.loc[:, 'des'].apply(lambda x: int('challenged' in x.lower() and 'overturned' in x.lower()))
        df_events.loc[:, 'challenge_upheld_flg'] = df_events.loc[:, 'des'].apply(lambda x: int('challenged' in x.lower() and 'upheld' in x.lower()))
        
        # Team that challenged
        df_events.loc[:, 'challenge_team'] = df_events.apply(lambda x: x['des'].split('challenged')[0].strip() if x['challenge_flg'] == 1 else None, axis=1)
        df_events.loc[:, 'challenge_team_id'] = df_events.apply(lambda x: self.df_team.loc[x['challenge_team'] == self.df_team['name_brief'], 'id'].iloc[0] if pd.notnull(x['challenge_team']) else np.NaN, axis=1)
        
        # Check the lengths... They should match. 
        # If not, throw an error as there were duplicates created.
        assert events_length == len(df_events), \
            'There were duplicate records created in df_events, %d != %d. Please check the dataframe for the source of duplicates.'%(events_length, len(df_events))
        
        # Re-sort the events
        # Sort order should be: inning, inning_type, tfs_zulu (timestamp), tag counter
        df_events.sort(columns=['inning', 'inning_type', 'tfs_zulu', 'tag_counter'], 
                       ascending=[True, False, True, True], inplace=True, na_position='first')
        df_events.reset_index(drop=True, inplace=True)
        
        # Save back to the instance        
        self.df_pitch = df_pitch
        self.df_runner = df_runner
        self.df_events = df_events
        self.df_hitchart = df_hitchart
        
        return self
        
    def _add_statcast_data(self, final_flg=True):
        """Preps the Statcast data from a JSON file.
        
        Args:
            final_flg (bln, optional): False if the game is not final and the events are not yet needed.
                Defaults to True.
            
        """
        # Save away values needed locally
        logger = self.logger
        game_pk = self.game_pk
        
        # Construct the JSON URL
        url = self.statcast_url.replace('{{game_pk}}', '%d'%(game_pk))
        
        # Try grabbing the JSON
        try:
            r = requests.get(url)
            data = r.json()['items']
        except Exception:
            # Assume no Statcast data exists.
            return self
        
        # Otherwise, continue processing
        data_list = []
        for item in data:
            if item['group'] == 'playByPlay' and item['guid'][:10] == 'playResult':
                item_dict = dict()                
                item_dict.update({'atbat_num': int(item['guid'][11:].strip())})
                item_dict.update({'player_id': item['data']['player_id']})
                
                # Parse out the description for Statcast data
                des = item['data']['description'].replace('.', '').replace(',', '').replace(';', '').split()
                
                try:
                    exit_speed = int(des[des.index('mph') - 1])
                except Exception:
                    exit_speed = np.NaN
                
                try:
                    launch_angle = int(des[des.index('degrees') - 1])
                except Exception:
                    launch_angle = np.NaN
                    
                try:
                    distance = int(des[des.index('feet') - 1])
                except Exception:
                    distance = np.NaN
                
                # Update the dict and append to the data
                item_dict.update({'exit_speed': exit_speed, 'launch_angle': launch_angle, 'travel_distance': distance})                
                data_list.append(item_dict)
                
        self.df_statcast = pd.DataFrame(data_list)
        
        return self
        
    def prep_all(self):
        # Save away values needed locally
        logger = self.logger
        game_id = self.game_id
        
        # Error value and final_flg to keep track
        error_value = ''
        final_flg = False
        
        # Run the prep functions
        try:
            self._prep_game_xml()
        except:
            #raise
            logger.exception('Error in getting game XML data. Game ID: %s'%(game_id))
            error_value = 'game'
        
        try:
            if error_value == '':
                self._prep_players_xml()
        except:
            #raise
            logger.exception('Error in getting players XML data. Game ID: %s'%(game_id))
            error_value = 'players'
        
        try:
            if error_value == '':
                self._prep_boxscore_xml()
        except:
            #raise
            logger.exception('Error in getting boxscore XML data. Game ID: %s'%(game_id))            
            error_value = 'boxscore'
        else:
            if error_value == '':
                # Get the final flag, and game pk
                final_flg = self.final_flg
                game_pk = self.game_pk
                
                # Set error value if game_pk is null
                if pd.isnull(game_pk):
                    error_value = 'game_pk'
                
        try:
            if error_value == '':        
                self._prep_events_xml(final_flg=final_flg)
        except:
            #raise
            logger.exception('Error in getting events XML data. Game ID: %s'%(game_id))
            error_value = 'events'
            
        try:
            if error_value == '':        
                self._add_statcast_data(final_flg=final_flg)
        except:
            #raise
            logger.exception('Error in getting Statcast data. Game ID: %s'%(game_id))
            error_value = 'statcast'
        
        # Continue processing if no errors
        if error_value == '':
            # Add in the game information
            self.df_game.loc[:, 'game_pk'] = game_pk
            self.df_game.loc[:, 'game_id'] = game_id
            
            # Do the below merging with Statcast data only after the game goes final.
            if final_flg == True:
                # Merge in Statcast data if exists and length > 0
                if hasattr(self, 'df_statcast') and len(self.df_statcast) > 0:
                    # Check to see whether the max atbat numbers are equal...
                    if cf.convert_to_int(np.nanmax(list(self.df_statcast['atbat_num']))) != cf.convert_to_int(np.nanmax(list(self.df_events['atbat_num']))):
                        warnings.warn('Number of atbats do not match across Statcast and Events data in ID %s.'%(self.game_id))
                    
                    # Merge in the Statcast data
                    df_events = pd.merge(self.df_events, self.df_statcast, on=['atbat_num', 'player_id'], how='left')
                    
                    # Check the lengths... They should match. 
                    # If not, throw an error as there were duplicates created.
                    assert len(self.df_events) == len(df_events), \
                        'There were duplicate records created in df_events due to Statcast data. Please check the dataframe for the source of duplicates.'
                    
                    # Re-sort the events
                    # Sort order should be: inning, inning_type, tfs_zulu (timestamp), tag counter
                    df_events.sort(columns=['inning', 'inning_type', 'tfs_zulu', 'tag_counter'], 
                                   ascending=[True, False, True, True], inplace=True, na_position='first')
                    df_events.reset_index(drop=True, inplace=True)
                    
                else:
                    # Create the new dummy columns for Statcast
                    df_events = self.df_events
                    df_events.loc[:, 'exit_speed'] = np.NaN
                    df_events.loc[:, 'launch_angle'] = np.NaN
                    df_events.loc[:, 'travel_distance'] = np.NaN      
                    
                # Save back the events dataframe back to the instance
                self.df_events = df_events
                
                # Save away the players dataframe locally to use
                df_players = self.df_players
                
                # Rename some columns
                df_players.rename(columns={'id': 'player_id', 'num': 'player_num'}, inplace=True)
                
                # Boxscore -- game results
                df_game_results = self.df_boxscore
                df_game_results = pd.merge(df_game_results, self.df_game.loc[:, ['game_pk', 'home_team_id', 'away_team_id']], on='game_pk', how='left')
                
                # Game status
                df_game_results.loc[:, 'final_flg'] = int(final_flg)
            
                # Scores
                df_players.loc[:, 'r_bat'] = df_players.loc[:, 'r_bat'].apply(cf.convert_to_int)
                df_players_groupby = df_players.groupby('team_id')
                df_runs = df_players_groupby['r_bat'].agg({'runs': np.sum})
                df_win = df_players_groupby['win'].agg({'win': lambda x: len(x[pd.notnull(x)])})
                df_loss = df_players_groupby['loss'].agg({'loss': lambda x: len(x[pd.notnull(x)])})
                df_save = df_players_groupby['save'].agg({'save': lambda x: len(x[pd.notnull(x)])})
                df_blown_save = df_players_groupby['blown_save'].agg({'blown_saves': lambda x: len(x[pd.notnull(x)])})
                df_holds = df_players_groupby['hold'].agg({'holds': lambda x: len(x[pd.notnull(x)])})
                
                df_team_stats = df_runs.join(df_win, how='inner')
                df_team_stats = df_team_stats.join(df_loss, how='inner')
                df_team_stats = df_team_stats.join(df_save, how='inner')
                df_team_stats = df_team_stats.join(df_blown_save, how='inner')
                df_team_stats = df_team_stats.join(df_holds, how='inner')
                df_team_stats.reset_index(inplace=True)
                
                # Placeholder columns
                df_game_results.loc[:, 'team_id'] = np.NaN
                df_game_results.loc[:, 'runs'] = np.NaN
                df_game_results.loc[:, 'win'] = np.NaN
                df_game_results.loc[:, 'loss'] = np.NaN
                df_game_results.loc[:, 'save'] = np.NaN
                df_game_results.loc[:, 'blown_saves'] = np.NaN
                df_game_results.loc[:, 'holds'] = np.NaN
                
                df_game_results = pd.merge(df_game_results, df_team_stats, left_on='home_team_id', right_on='team_id', how='left', suffixes=['', '_home'])
                df_game_results = pd.merge(df_game_results, df_team_stats, left_on='away_team_id', right_on='team_id', how='left', suffixes=['', '_away'])
                
                # Drop the placeholder columns
                df_game_results.drop(labels=['team_id', 'team_id_home', 'team_id_away', 'home_team_id', 'away_team_id', 'runs', 'win', 'loss', 'save', 'blown_saves', 'holds'], inplace=True, axis=1)
                
            else:
                df_players = pd.DataFrame()
                df_game_results = pd.DataFrame()
            
            # Cut some dataframes to needed columns
            df_team = self.df_team.loc[:, ['id', 'code', 'abbrev', 'league', 'name_full', 'name_brief']]
            df_coach = self.df_coach.loc[:, ['id', 'first', 'last', 'team_id', 'position']]
            df_umpire = self.df_umpire
            
            # Drop ID 0's
            df_umpire = df_umpire.loc[df_umpire['id'] != '0', :]
            df_coach = df_coach.loc[df_coach['id'] != '0', :]
            
            # Rename some columns for the final tables...
            df_stadium = self.df_stadium
            df_coach.rename(columns={'id': 'coach_id'}, inplace=True)
            df_umpire.rename(columns={'id': 'umpire_id'}, inplace=True)
            df_stadium.rename(columns={'id': 'stadium_id'}, inplace=True)
            df_team.rename(columns={'id': 'team_id'}, inplace=True)
            
            # Add in the game pk for all relevant df's
            if len(df_coach) > 0:
                df_coach.loc[:, 'game_pk'] = game_pk
            
            if len(df_players) > 0:
                df_players.loc[:, 'game_pk'] = game_pk
                
            if len(df_umpire) > 0:
                df_umpire.loc[:, 'game_pk'] = game_pk
            
            if hasattr(self, 'df_events') and len(self.df_events) > 0:
                self.df_events.loc[:, 'game_pk'] = game_pk
                
            if hasattr(self, 'df_pitch') and len(self.df_pitch) > 0:
                self.df_pitch.loc[:, 'game_pk'] = game_pk
            
            # Save back to the instance
            self.df_players = df_players
            self.df_coach = df_coach
            self.df_umpire = df_umpire
            self.df_stadium = df_stadium
            self.df_team = df_team
            self.df_game_results = df_game_results            
            
        self.error_value = error_value
        self.final_flg = final_flg

        return self
        

if __name__ == '__main__':
    #game_id = 'gid_2014_05_01_atlmlb_miamlb_1'
    #game_id = 'gid_2015_04_14_miamlb_atlmlb_1'
    #game_id = 'gid_2013_02_27_houmlb_tormlb_1'
    #game_id = 'gid_2014_10_07_ndomlb_nwcmlb_1'
    #game_id = 'gid_2010_05_11_phimlb_colmlb_1'
    #game_id = 'gid_2011_03_18_atlmlb_nynmlb_1'
    #game_id = 'gid_2011_03_29_ccubbc_texmlb_1'
    #game_id = 'gid_2010_04_01_atlmlb_detmlb_1'
    #game_id = 'gid_2010_03_02_atlmlb_nynmlb_1'
    #game_id = 'gid_2010_03_07_chnmlb_chamlb_1'
    #game_id = 'gid_2010_05_09_tbamlb_oakmlb_1'
    #game_id = 'gid_2010_03_13_lanmlb_arimlb_1'
    #game_id = 'gid_2014_03_22_lanmlb_arimlb_1'
    #game_id = 'gid_2011_05_26_bosmlb_detmlb_1'
    #game_id = 'gid_2015_08_01_atlmlb_phimlb_1'
    #game_id = 'gid_2015_07_17_chnmlb_atlmlb_1'
    #game_id = 'gid_2015_08_08_chamlb_kcamlb_1'
    #game_id = 'gid_2014_02_25_fanbbc_nyamlb_1'
    #game_id = 'gid_2014_06_29_colmlb_milmlb_1'
    #game_id = 'gid_2015_03_09_clemlb_seamlb_1'
    game_id = 'gid_2015_04_17_phimlb_wasmlb_1'
    game_id = 'gid_2015_08_12_anamlb_chamlb_1'
    
    sg = SingleGame(game_id)
    sg.prep_all()
    
    """
    # Create some samples...
    sg.df_game.to_csv('../output/df_game_sample.csv', index=False, sep=',')
    sg.df_events.to_csv('../output/df_events_sample.csv', index=False, sep=',')
    sg.df_pitch.to_csv('../output/df_pitch_sample.csv', index=False, sep=',')
    sg.df_players.to_csv('../output/df_players_sample.csv', index=False, sep=',')
    sg.df_game_results.to_csv('../output/df_game_results_sample.csv', index=False, sep=',')
    """